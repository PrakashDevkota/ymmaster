﻿using MasterInterface.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MasterInterface.Interface
{
  public  interface IMenuService
    {
        List<MenuModel> GetAllActiveMenus(int RoleId);
        List<MenuModel> GetMainMenuList(int RoleId);
        //List<MenuModel> GetDashBoardSetupMenuList(int RoleId);
        //List<MenuModel> GetDashBoardStudentMenuList(int RoleId);
        //List<MenuModel> GetDashBoardExamCenterMenuList(int RoleId);
        //List<MenuModel> GetDashBoardCourseMenuList(int RoleId);

    }
}

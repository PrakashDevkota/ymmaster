﻿using MasterInterface.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace MasterInterface.Interface
{
public interface IStudentService
    {
        #region student
        List<StudentModel> GetAllStudents();
        List<StudentModel> GetAllBackPaperStudentList();
        List<StudentModel> GetAllStudentByInstituteBodyId(int id);

        void AddStudent(StudentModel StudentModel);
        StudentModel GetOneStudent(int id);
        void UpdateStudent(StudentModel StudentModel);
        IEnumerable<SelectListItem> GetInsituteBodyList();
        IEnumerable<SelectListItem> GetCourseFeildOfStudyList();
        IEnumerable<SelectListItem> GetCourseDegreeList();
     
        IEnumerable<SelectListItem> GetStudentSexSelectList();
        

        #endregion
        #region student previous degree
        List<StudentPreviousDegreeModel> GetAllStudentPreviousDegrees();
        void AddStudentPreviousDegree(StudentPreviousDegreeModel StudentPreviousDegreeModel);
        StudentPreviousDegreeModel GetOneStudentPreviousDegree(int id);
        void UpdateStudentPreviousDegree(StudentPreviousDegreeModel StudentPreviousDegreeModel);
        IEnumerable<SelectListItem> GetGetCourseInstituteBodyList();
        IEnumerable<SelectListItem> GetCourseDegreeLevelList();
        List<StudentModel> GetAllFilteredStudent(StudentWithFilter record);
        #endregion
    }
}

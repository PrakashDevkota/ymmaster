﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MasterInterface.Model;

namespace MasterInterface.Interface
{
    public interface IExamCenter
    {
        List<ExamCenterMasterModel> GetExamCenterByExamId(int ExamId);
        void CreateOrUpdateExamCenters(ExamCenterMasterModelList record);
        List<ExamCenterMasterModel> GetExamCenters(int examId);
        ExamCenterCompleteModel GetExamCenterByDetailId(int id);
        void CreateOrUpdateAssignCollege(ExamCenterCompleteModel record, out bool Succeded, out string Message);
    }

}

﻿using MasterInterface.Model;
using MasterRepository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MasterInterface.Interface
{
   public  interface ICommonService
    {
        int GetMenuId(string controllername);
        RoleAccess GetRoleAccessByMenuIdAndRoleId(int menuId, int roleId);
     
    }
}

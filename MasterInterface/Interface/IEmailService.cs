﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MasterInterface.Model;
namespace MasterInterface.Interface
{
   public interface IEmailService
    {
        void SendMail(UserModel Record);
    }
}

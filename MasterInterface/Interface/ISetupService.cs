﻿using MasterInterface.Model;
using MasterRepository.UnitOfWork;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace MasterInterface.Interface
{
    public interface ISetupService
    {
        #region Menu
        List<MenuModel> GetAllMenus();


        IEnumerable<SelectListItem> GetParentMenus();
        void AddMenu(MenuModel menuModel);
        MenuModel GetOneMenu(int id);
        void UpdateMenu(MenuModel menuModel);
        #endregion

        #region Role
        List<RoleModel> GetAllRoles();
        void AddRole(RoleModel RoleModel);
        RoleModel GetOneRole(int id);
        void UpdateRole(RoleModel RoleModel);
        #endregion

        #region User
        void AddUser(UserModel userModel);
        AssignMenuModel GetRoleAccess(int id);
       // IEnumerable<SelectListItem> GetRoleSelectList();
        IEnumerable<SelectListItem> GetInstiteBodySelectList();
        void AddRoleAccess(AssignMenuModel Record);

        #endregion

    }
}

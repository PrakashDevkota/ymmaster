﻿using MasterInterface.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace MasterInterface.Interface
{
 public   interface IExamService
    {
        #region Examdeclare
        List<ExamDeclareModel> GetAllExamDeclares();
        void AddExamDeclare(ExamDeclareModel ExamDeclareModel);
        ExamDeclareModel GetOneExamDeclare(int id);
        void UpdateExamDeclare(ExamDeclareModel ExamDeclareModel);
        List<SelectListItem> GetExamDeclareSelectList();
        IEnumerable<SelectListItem> GetAllExamDeclareSelectList();
        #endregion

        #region Exam Registration
        List<ExamDeclareModel> GetActiveRegularExamDeclareList();
        List<ExamDeclareModel> GetActiveBackExamDeclareList();

        #endregion

        #region ExamRegistrationMaster
        List<ExamRegistrationMasterModel> GetAllExamRegistrationMasters();
        List<ExamRegistrationMasterModel> GetUniqueCollegeExamRegistrationMaster();
        void AddRegularExamRegistrationMasterAndExamRegistrationDetail(ExamRegistrationMasterModel ExamRegistrationMasterModel,int id);
        void AddBackExamRegistrationMasterAndExamRegistrationDetail(ExamRegistrationMasterModel ExamRegistrationMasterModel, int id);

        ExamRegistrationMasterModel GetOneExamRegistrationMaster(int id);
        void UpdateExamRegistrationMaster(ExamRegistrationMasterModel ExamRegistrationMasterModel);
        ExamRegistrationMasterModel GetRegularExamForm(int LoginId, int ExamDeclareId);
        ExamRegistrationMasterModel GetBackExamForm(int LoginId, int ExamDeclareId);
        #endregion

        #region Exam Fee Structure
        List<ExamFeeStructureModel> GetAllExamFeeStructures();
        void AddExamFeeStructure(ExamFeeStructureModel ExamFeeStructureModel);
        ExamFeeStructureModel GetOneExamFeeStructure(int id);
        void UpdateExamFeeStructure(ExamFeeStructureModel ExamFeeStructureModel);
        #endregion

        #region Exam Back Paper
        List<ExamBackPaperModel> GetAllExamBackPapers();
        List<ExamBackPaperModel> GetAllExamBackPaperByInstituteBodyId(int id);
        void AddExamBackPaper(ExamBackPaperModel ExamBackPaperModel);
        ExamBackPaperModel GetOneExamBackPaper(int id);
        void UpdateExamBackPaper(ExamBackPaperModel ExamBackPaperModel);
        IEnumerable<SelectListItem> GetExamBackPaperStudentSelectList();
        #endregion


        bool ValidateIfRegistered(int Id, int StudentId);
     
        
    }
}

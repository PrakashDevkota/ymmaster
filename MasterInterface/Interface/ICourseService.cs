﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using MasterInterface.Model;
using MasterRepository;

namespace MasterInterface.Interface
{
    public interface ICourseService
    {

       #region course

        List<CourseModel> GetAllCourses();
        IEnumerable<SelectListItem> GetParentCourses();
        IEnumerable<SelectListItem> GetCourseFieldOfStudyListSelectList();

        List<SelectListItem> GetCourseDegreeSelectList();
        void AddCourse(CourseModel courseModel);
        CourseModel GetOneCourse(int id);
        void UpdateCourse(CourseModel courseModel);
        void DeleteCourse(int id);
        bool IsCourseNameExists(string CourseCode);
        bool IsCourseCodeExists(string CourseName);

        #endregion
       #region course degree
        List<CourseDegreeModel> GetAllCourseDegrees();
        void AddCourseDegree(CourseDegreeModel courseDegreeModel);
        CourseDegreeModel GetOneCourseDegree(int id);
        void UpdateCourseDegree(CourseDegreeModel courseDegreeModel);

        bool IsCourseDegreeCodeExists(string CourseDegreeCode);
        bool IsCourseDegreeNameExists(string CourseDegreeName);
        #endregion
       #region course degree level
        List<CourseDegreeLevelModel> GetAllCourseDegreeLevel();
        void AddCourseDegreeLevel(CourseDegreeLevelModel courseDegreeLevelModel);
        CourseDegreeLevelModel GetOneCourseDegreeLevel(int id);
         void UpdateCourseDegreeLevel(CourseDegreeLevelModel courseDegreeLevelModel);
        bool IsCourseDegreeLevelCodeExists(string CourseDegreeLevelCode);
        bool IsCourseDegreeLevelNameExists(string CourseDegreeLevelName);
        IEnumerable<SelectListItem> GetCourseDegreeLevel();
        #endregion
       #region Field Of Study
        List<CourseFieldOfStudyModel> GetAllCourseFieldOfStudy();
        void AddCourseFieldOfStudy(CourseFieldOfStudyModel courseFieldOfStudyModel);
        CourseFieldOfStudyModel GetOneCourseFieldOfStudy(int id);
        void UpdateCourseFieldOfStudy(CourseFieldOfStudyModel courseFieldOfStudyModel);
        bool IsCourseFieldOfStudyCodeExists(string Code);
        bool IsCourseFieldOfStudyNameExists(string Name);
        #endregion
     
       #region course institute location
        List<CourseInstituteLocationModel> GetAllCourseInstituteLocation();
        void AddCourseInstituteLocation(CourseInstituteLocationModel courseInstituteLocationModel);
        CourseInstituteLocationModel GetOneCourseInstituteLocation(int id);
        void UpdateCourseInstituteLocation(CourseInstituteLocationModel courseInstituteLocationModel);
        bool IsCourseInstituteLocationNameExists(string CourseInstituteLocationName);
        bool ICourseInstituteLocationCodeExists(string CourseInstituteLocationeCode);
        #endregion
       #region course intitute body
        List<CourseInstituteBodyModel> GetAllCourseInstituteBody();
        IEnumerable<SelectListItem> GetCourseInstituteLocation();
        List<SelectListItem> GetCourseInstituteBodySelectList();
        IEnumerable<SelectListItem> GetCourseInstituteBodyTypeList();
        void AddCourseInstituteBody(CourseInstituteBodyModel courseInstituteBodyModel);
        CourseInstituteBodyModel GetOneCourseInstituteBody(int id);
        void UpdateCourseInstituteBody(CourseInstituteBodyModel courseInstituteBodyModel);
        bool IsCourseInstituteBodyCodeExists(string CourseInstituteBodyCode);
        bool IsCourseInstituteBodyNameExists(string CourseInstituteBodyName);
        #endregion
       #region course master
        List<CourseMasterModel> GetAllCourseMasters();
        IEnumerable<SelectListItem> GetCourseFeildOfStudyList();
        IEnumerable<SelectListItem> GetCourseDegreeList();
      
        void AddCourseMaster(CourseMasterModel CourseMasterModel);
        CourseMasterModel GetOneCourseMaster(int id);
        void UpdateCourseMaster(CourseMasterModel CourseMasterModel);
       
       
        #endregion
    }
}

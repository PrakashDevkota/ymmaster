﻿using MasterInterface.Model;
using MasterRepository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace MasterInterface.Interface
{
    public interface IUserService
    {

        #region Student User
        UserModel GetStudentUserModelCreateFor(int id);
        UserModel GetStudentUserModelUpdateFor(int id);

        void AddStudentUser(UserModel Record);
        void UpdateStudentUser(UserModel Record);
        void ResetStudentUser(int id);
        #endregion

        #region College User
        UserModel GetCollegeUserModelCreateFor(int id);
        UserModel GetCollegeUserModelUpdateFor(int id);
        void AddCollegeUser(UserModel Record);
        void UpdateCollegeUser(UserModel Record);
        void ResetCollegeUser(int id);
        #endregion




        IEnumerable<SelectListItem> GetStudentSelectList();

    }
}

﻿using MasterInterface.Helper;
using MasterInterface.Interface;
using MasterInterface.Model;
using MasterRepository;
using MasterRepository.UnitOfWork;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace MasterInterface.Implementation
{
 public class StudentService: IStudentService
    {

        #region Constructor
        private readonly IUnitOfWork _unitOfWork;
        public StudentService(IUnitOfWork unitOfWork = null)
        {
            _unitOfWork = unitOfWork ?? new UnitOfWork();
        }


        #endregion
        #region student
        public List<StudentModel> GetAllStudentByInstituteBodyId(int id)
        {
            List<Student> Record = _unitOfWork.StudentRepository.Get(x=>x.InstituteBodyId == id && x.CompleteFlag==false).OrderBy(x=>x.InstituteBodyId).ToList();
            List<StudentModel> ModelRecord = Mapper.StudentMapper.StudentDbToModelList(Record);
            foreach (StudentModel item in ModelRecord)
            {
                LoginStudent LoginRecord = _unitOfWork.LoginStudentRepository.Get(x => x.StudentId == item.Id).FirstOrDefault();
                if (LoginRecord != null)
                {
                    item.IsUserCreated = true;
                }
            }
            return ModelRecord;
        }
        public List<StudentModel> GetAllFilteredStudent(StudentWithFilter record)
        {
            string Query = "select * from Student where InstituteBodyId = 1 and CompleteFlag = 0";
            if(record.CourseDegreeId != null)
            {
                Query = Query + " and DegreeId = " + record.CourseDegreeId.ToString();
            }
            if(record.StudentId != null)
            {
                Query = Query + " and Id = " + record.StudentId.ToString();
            }
            List<Student> Record = _unitOfWork.StudentRepository.GetWithRawSql(Query).ToList();
            List<StudentModel> ModelRecord = Mapper.StudentMapper.StudentDbToModelList(Record);
            return ModelRecord;
        }
        public List<StudentModel> GetAllBackPaperStudentList()
        {
            List<Student> Record = _unitOfWork.StudentRepository.Get(x=>x.CompleteFlag==false).ToList();
            List<StudentModel> ModelRecord = Mapper.StudentMapper.StudentDbToModelList(Record);
            return ModelRecord;

        }
        public List<StudentModel> GetAllStudents()
        {
            List<Student> Record = _unitOfWork.StudentRepository.All().OrderBy(x => x.InstituteBodyId).ToList();
            List<StudentModel> ModelRecord = Mapper.StudentMapper.StudentDbToModelList(Record);
            foreach (StudentModel item in ModelRecord)
            {
                LoginStudent LoginRecord = _unitOfWork.LoginStudentRepository.Get(x => x.StudentId == item.Id).FirstOrDefault();
                if (LoginRecord != null)
                {
                    item.IsUserCreated = true;
                }
            }
            return ModelRecord;
        }



        public void AddStudent(StudentModel StudentModel)
        {
            Student Record = Mapper.StudentMapper.StudentModelToDb(StudentModel);
            _unitOfWork.StudentRepository.Create(Record);
        }

        public void UpdateStudent(StudentModel StudentModel)
        {
            Student Record = Mapper.StudentMapper.StudentModelToDb(StudentModel);
            _unitOfWork.StudentRepository.Update(Record);
        }

        public StudentModel GetOneStudent(int id)
        {
            Student Record = _unitOfWork.StudentRepository.GetById(id);
            StudentModel ReturnRecord = Mapper.StudentMapper.StudentDbToModel(Record);
            return ReturnRecord;
        }

        public IEnumerable<SelectListItem> GetInsituteBodyList()
        {
            List<CourseInstituteBody> Record = _unitOfWork.CourseInstituteBodyRepository.All().ToList();
            List<SelectListItem> ReturnRecord = new List<SelectListItem>();
            foreach (var row in Record)
            {
                ReturnRecord.Add(new SelectListItem
                {
                    Text = row.InstituteBodyName,
                    Value = row.Id.ToString()
                });
            }
            return ReturnRecord;
        }

        public IEnumerable<SelectListItem> GetCourseFeildOfStudyList()
        {
            List<CourseFieldOfStudy> Record = _unitOfWork.CourseFieldOfStudyRepository.All().ToList();
            List<SelectListItem> ReturnRecord = new List<SelectListItem>();
            foreach (var row in Record)
            {
                ReturnRecord.Add(new SelectListItem
                {
                    Text = row.Name,
                    Value = row.Id.ToString()
                });
            }
            return ReturnRecord;
        }

        public IEnumerable<SelectListItem> GetCourseDegreeList()
        {
            List<CourseDegree> Record = _unitOfWork.CourseDegreeRepository.All().ToList();
            List<SelectListItem> ReturnRecord = new List<SelectListItem>();
            foreach (var row in Record)
            {
                ReturnRecord.Add(new SelectListItem
                {
                    Text = row.DegreeName,
                    Value = row.Id.ToString()
                });
            }
            return ReturnRecord;
        }

        public IEnumerable<SelectListItem> GetStudentSexSelectList()
        {
             return StaticSelectlist.GetGenderSelectList();
        }

        #endregion
        #region student previous degree

        public List<StudentPreviousDegreeModel> GetAllStudentPreviousDegrees()
        {
            List<StudentPreviousDegree> Record = _unitOfWork.StudentPreviousDegreeRepository.All().ToList();
            List<StudentPreviousDegreeModel> ModelRecord = Mapper.StudentPreviousDegreeMapper.StudentPreviousDegreeDbToModelList(Record);
            return ModelRecord;
        }


        public void AddStudentPreviousDegree(StudentPreviousDegreeModel StudentPreviousDegreeModel)
        {
            StudentPreviousDegree Record = Mapper.StudentPreviousDegreeMapper.StudentPreviousDegreeModelToDb(StudentPreviousDegreeModel);
            _unitOfWork.StudentPreviousDegreeRepository.Create(Record);
        }

        public void UpdateStudentPreviousDegree(StudentPreviousDegreeModel StudentPreviousDegreeModel)
        {
            StudentPreviousDegree Record = Mapper.StudentPreviousDegreeMapper.StudentPreviousDegreeModelToDb(StudentPreviousDegreeModel);
            _unitOfWork.StudentPreviousDegreeRepository.Update(Record);
        }

        public StudentPreviousDegreeModel GetOneStudentPreviousDegree(int id)
        {
            StudentPreviousDegree Record = _unitOfWork.StudentPreviousDegreeRepository.GetById(id);
            StudentPreviousDegreeModel ReturnRecord = Mapper.StudentPreviousDegreeMapper.StudentPreviousDegreeDbToModel(Record);
            return ReturnRecord;
        }

        public IEnumerable<SelectListItem> GetGetCourseInstituteBodyList()
        {
            List<CourseInstituteBody> Record = _unitOfWork.CourseInstituteBodyRepository.All().ToList();
            List<SelectListItem> ReturnRecord = new List<SelectListItem>();
            foreach (var row in Record)
            {
                ReturnRecord.Add(new SelectListItem
                {
                    Text = row.InstituteBodyName,
                    Value = row.Id.ToString()
                });
            }
            return ReturnRecord;
        }

        public IEnumerable<SelectListItem> GetCourseDegreeLevelList()
        {
            List<CourseDegreeLevel> Record = _unitOfWork.CourseDegreeLevelRepository.All().ToList();
            List<SelectListItem> ReturnRecord = new List<SelectListItem>();
            foreach (var row in Record)
            {
                ReturnRecord.Add(new SelectListItem
                {
                    Text = row.DegreeLevelName,
                    Value = row.Id.ToString()
                });
            }
            return ReturnRecord;
        }

        #endregion
    }
}




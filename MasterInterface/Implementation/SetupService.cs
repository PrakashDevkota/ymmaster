﻿using MasterInterface.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MasterInterface.Model;
using System.Web.Mvc;
using MasterRepository.UnitOfWork;
using MasterRepository;
using System.Transactions;


namespace MasterInterface.Implementation
{
    public class SetupService : ISetupService
    {
        #region constructor
        private readonly IUnitOfWork _unitOfWork;
        public SetupService(UnitOfWork unityOfWork = null)
        {
            _unitOfWork = unityOfWork ?? new UnitOfWork();
        }
        #endregion
        #region Menu
        public void AddMenu(MenuModel menuModel)
        {
            Menu Record = Mapper.MenuMapper.MenuModelToDb(menuModel);
            _unitOfWork.MenuRepository.Create(Record);
        }

        public List<MenuModel> GetAllMenus()
        {
            List<Menu> Record = _unitOfWork.MenuRepository.All().ToList();
            List<MenuModel> ReturnRecord = Mapper.MenuMapper.MenuDbToModelList(Record);
            return ReturnRecord;
        }
    

        public MenuModel GetOneMenu(int id)
        {
            Menu Record = _unitOfWork.MenuRepository.GetById(id);
            MenuModel ReturnRecord = Mapper.MenuMapper.MenuDbToModel(Record);
            return ReturnRecord;

        }

        public IEnumerable<SelectListItem> GetParentMenus()
        {
            List<SelectListItem> pmsl = new List<SelectListItem>();
            List<MenuModel> modelMenu = GetAllMenus();
            foreach (var Record in modelMenu)
            {
                pmsl.Add(new SelectListItem
                {
                    Text = Record.Name,
                    Value = Record.Id.ToString()
                });

            }
            return pmsl;

        }

        public void UpdateMenu(MenuModel menuModel)
        {
            Menu Record = Mapper.MenuMapper.MenuModelToDb(menuModel);
            _unitOfWork.MenuRepository.Update(Record);
        }
        #endregion
        #region Role
        public void AddRole(RoleModel RoleModel)
        {
            Role Record = Mapper.RoleMapper.RoleModelToDb(RoleModel);
            _unitOfWork.RoleRepository.Create(Record);
        }

        public List<RoleModel> GetAllRoles()
        {
            List<Role> Record = _unitOfWork.RoleRepository.All().ToList();
            List<RoleModel> ReturnRecord = Mapper.RoleMapper.RoleDbToModelList(Record);
            return ReturnRecord;
        }

        public RoleModel GetOneRole(int id)
        {
            Role Record = _unitOfWork.RoleRepository.GetById(id);
            RoleModel ReturnRecord = Mapper.RoleMapper.RoleDbToModel(Record);
            return ReturnRecord;

        }

        public void UpdateRole(RoleModel RoleModel)
        {
            Role Record = Mapper.RoleMapper.RoleModelToDb(RoleModel);
            _unitOfWork.RoleRepository.Update(Record);
        }
            #endregion
        #region User

        public void AddUser(UserModel userModel)
        {
          //Login Record=Mapper.UserMapper.UserModelToDb(userModel);
          //  _unitOfWork.LoginRepository.Create(Record);
        }

        //public IEnumerable<SelectListItem> GetRoleSelectList()
        //{
        //     List<Student> Record = _unitOfWork.StudentRepository.All().ToList();
        //    List<SelectListItem> ReturnRecord = new List<SelectListItem>();
        //    foreach (var row in Record)
        //    {
        //        ReturnRecord.Add(new SelectListItem
        //        {
        //            Text = row.FirstName,
        //            Value = row.Id.ToString()
        //        });
        //    }
        //    return ReturnRecord;
        //}

        public IEnumerable<SelectListItem> GetInstiteBodySelectList()
        {
            throw new NotImplementedException();
        }

      
        public AssignMenuModel GetRoleAccess(int id)
        {
            AssignMenuModel Record = new AssignMenuModel();
            List<MenuModel> MenuModelList = new List<MenuModel>();
            List<Menu> menu = _unitOfWork.MenuRepository.All().ToList();
            foreach(Menu Item in menu)
            {
                RoleAccess RoleAccess = _unitOfWork.RoleAccessRepository.Get(x => x.RoleId == id && x.MenuId == Item.Id).FirstOrDefault();
                MenuModel MenuRecord = new MenuModel();
                if (RoleAccess != null)
                {
                    MenuRecord.Id = RoleAccess.MenuId;
                    MenuRecord.IsSelected = true;
                    MenuRecord.Name = Item.Name;
                    MenuRecord.Create = RoleAccess.AllowAdd;
                    MenuRecord.Read = RoleAccess.AllowView;
                    MenuRecord.Update = RoleAccess.AllowEdit;
                    MenuRecord.Delete = RoleAccess.AllowDelete;
                }
                else
                {
                    MenuRecord.Id = Item.Id;
                    MenuRecord.Name = Item.Name;
                 }
                MenuModelList.Add(MenuRecord);
            }
            Record.Menu = MenuModelList;
            Record.RoleId = id;
            return Record;

        }

        public void AddRoleAccess(AssignMenuModel record)
        {
          using (TransactionScope Scope = new TransactionScope())
            {
                _unitOfWork.RoleAccessRepository.Delete(x => x.RoleId == record.RoleId);
                foreach (MenuModel Item in record.Menu)
                {
                    if(Item.Create || Item.Read || Item.Update || Item.Delete)
                    {
                        RoleAccess RoleRecord = new RoleAccess();
                        RoleRecord.RoleId = record.RoleId;
                        RoleRecord.MenuId = Item.Id;
                        RoleRecord.AllowAdd = Item.Create;
                        RoleRecord.AllowView = Item.Read;
                        RoleRecord.AllowEdit = Item.Update;
                        RoleRecord.AllowDelete = Item.Delete;
                        _unitOfWork.RoleAccessRepository.Create(RoleRecord);
                    }                    
                }
                Scope.Complete();
            }
        }

        #endregion
    }
   }

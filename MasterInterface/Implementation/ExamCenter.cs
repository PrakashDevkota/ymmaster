﻿using MasterInterface.Interface;
using MasterInterface.Mapper;
using MasterInterface.Model;
using IOERepository;
using IOERepository.UnitOfWork;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Transactions;

namespace MasterInterface.Implementation
{
    public class ExamCenter : IExamCenter
    {
        #region Constructor
        private readonly IUnitOfWork _unitOfWork;
        public ExamCenter(IUnitOfWork unitOfWork = null)
        {
            _unitOfWork = unitOfWork ?? new UnitOfWork();
        }
        public List<ExamCenterMasterModel> GetExamCenterByExamId(int ExamId)
        {
            List<ExamCenterMasterModel> ReturnRecord = new List<ExamCenterMasterModel>();
            List<CourseInstituteBody> Record = _unitOfWork.CourseInstituteBodyRepository.All().ToList();
            foreach(CourseInstituteBody Item in Record)
            {
                ExamCenterMaster PastRecord = _unitOfWork.ExamCenterMasterRepository.Get(x => x.CenterCollegeId == Item.Id && x.ExamId == ExamId).FirstOrDefault();
                if(PastRecord != null)
                {
                    ExamCenterMasterModel PastRecordModel = ExamCenterMasterMapper.ExamCenterMasterDbToModel(PastRecord);
                    PastRecordModel.IsSelected = true;
                    ReturnRecord.Add(PastRecordModel);
                }
                else
                {
                    ExamDeclareModel ExamDeclare = ExamDeclareMapper.ExamDeclareDbToModel(_unitOfWork.ExamDeclareRepository.GetById(ExamId));
                    CourseInstituteBodyModel Institute = CourseInstituteBodyMapper.CourseInstituteBodyDbToModel(_unitOfWork.CourseInstituteBodyRepository.GetById(Item.Id));
                    ExamCenterMasterModel Singles = new ExamCenterMasterModel();
                    Singles.ExamId = ExamId;
                    Singles.CenterCollegeId = Item.Id;
                    Singles.Capacity = 0;
                    Singles.AllocatedStudentCount = 0;
                    Singles.IsSelected = false;
                    Singles.ExamDeclare = ExamDeclare;
                    Singles.CourseInstituteBody = Institute;
                    ReturnRecord.Add(Singles);
                }
            }
            return ReturnRecord;
        }
        public void CreateOrUpdateExamCenters(ExamCenterMasterModelList Record)
        {
            using (TransactionScope Scope = new TransactionScope())
            {
                foreach(ExamCenterMasterModel Item in Record.ExamCenterList)
                {
                    ExamCenterMaster PastRecord = _unitOfWork.ExamCenterMasterRepository.Get(x => x.CenterCollegeId == Item.CenterCollegeId && x.ExamId == Item.ExamId).FirstOrDefault();
                    if (Item.IsSelected)
                    {                        
                        if(PastRecord != null)
                        {
                            PastRecord.Capacity = Item.Capacity;
                            _unitOfWork.ExamCenterMasterRepository.Update(PastRecord);
                        }else
                        {
                            ExamCenterMaster NewRecord = ExamCenterMasterMapper.ExamCenterMasterModelToDb(Item);
                            NewRecord.AllocatedStudentCount = 0;
                            _unitOfWork.ExamCenterMasterRepository.Create(NewRecord);
                        }
                    }else
                    {
                        if(PastRecord != null)
                        {
                            _unitOfWork.ExamCenterMasterRepository.Delete(PastRecord);
                            _unitOfWork.Save();
                        }
                    }
                }
                 Scope.Complete();
            }
        }
        public List<ExamCenterMasterModel> GetExamCenters(int ExamId)
        {
            List<ExamCenterMaster> PastRecord = _unitOfWork.ExamCenterMasterRepository.Get(x => x.ExamId == ExamId).ToList();
            List<ExamCenterMasterModel> PastRecordModel = ExamCenterMasterMapper.ExamCenterMasterDbToModelList(PastRecord);
            return PastRecordModel;
        }
        public ExamCenterCompleteModel GetExamCenterByDetailId(int id)
        {
            ExamCenterMaster MasterRecord = _unitOfWork.ExamCenterMasterRepository.GetById(id);
            List<ExamCenterDetailModel> DetailRecord = new List<ExamCenterDetailModel>();
            ExamCenterCompleteModel Record = new ExamCenterCompleteModel();
            List<CourseInstituteBody> Institutes = _unitOfWork.CourseInstituteBodyRepository.All().ToList();
            foreach(CourseInstituteBody Item in Institutes)
            {
                ExamCenterDetailModel ExamCenterDetail = new ExamCenterDetailModel(); 
                ExamCenterDetail Singles = _unitOfWork.ExamCenterDetailRepository.Get(x=>x.ExamCenterId == id && x.ExamCollegeId == Item.Id).FirstOrDefault();
                if(Singles != null)
                {
                    ExamCenterDetail = ExamCenterDetailMapper.ExamCenterDetailDbToModel(Singles);
                    ExamCenterDetail.IsSelected = true;
                    ExamCenterDetail.StudentCount = Convert.ToInt32(_unitOfWork.ExamRegistrationMasterRepository.Get(x => x.ExamDeclareId == MasterRecord.ExamId && x.InstituteBodyId == Item.Id).Count());
                    ExamCenterDetail.ExamCollegeId = Item.Id;
                    ExamCenterDetail.CourseInstituteBody = CourseInstituteBodyMapper.CourseInstituteBodyDbToModel(_unitOfWork.CourseInstituteBodyRepository.GetById(ExamCenterDetail.CenterCollegeId));
                    ExamCenterDetail.CourseInstituteBody1 = CourseInstituteBodyMapper.CourseInstituteBodyDbToModel(_unitOfWork.CourseInstituteBodyRepository.GetById(ExamCenterDetail.ExamCollegeId));
                }
                else
                {
                    ExamCenterDetail.IsSelected = false;
                    ExamCenterDetail.CenterCollegeId = MasterRecord.CenterCollegeId;
                    ExamCenterDetail.ExamCollegeId = Item.Id;
                    ExamCenterDetail.CenterCapacity = MasterRecord.Capacity;
                    int StudentCount = _unitOfWork.ExamRegistrationMasterRepository.Get(x => x.ExamDeclareId == MasterRecord.ExamId && x.InstituteBodyId == Item.Id).Count();
                    ExamCenterDetail.StudentCount = Convert.ToInt32(StudentCount);
                    ExamCenterDetail.CourseInstituteBody = CourseInstituteBodyMapper.CourseInstituteBodyDbToModel(_unitOfWork.CourseInstituteBodyRepository.GetById(MasterRecord.CenterCollegeId));
                    ExamCenterDetail.CourseInstituteBody1 = CourseInstituteBodyMapper.CourseInstituteBodyDbToModel(_unitOfWork.CourseInstituteBodyRepository.GetById(Item.Id));
                }
                DetailRecord.Add(ExamCenterDetail);
            }
            Record.ExamCenterMaster = ExamCenterMasterMapper.ExamCenterMasterDbToModel(MasterRecord);
            Record.ExamCenterDetail = DetailRecord;
            return Record;
        }
        public void CreateOrUpdateAssignCollege(ExamCenterCompleteModel record,out bool Succeded,out string Message)
        {
            Succeded = false;
            Message = null;
            int TotalStudentCount = 0;
            using (TransactionScope Scope = new TransactionScope())
            {
                foreach (ExamCenterDetailModel Item in record.ExamCenterDetail)
                {
                    ExamCenterDetail PastRecord = _unitOfWork.ExamCenterDetailRepository.Get(x => x.CenterCollegeId == record.ExamCenterMaster.CenterCollegeId && x.ExamCollegeId == Item.ExamCollegeId).FirstOrDefault();
                    if (PastRecord != null)
                    {
                        if (Item.IsSelected)
                        {
                            PastRecord.StudentCount = Item.StudentCount;
                            _unitOfWork.ExamCenterDetailRepository.Update(PastRecord);
                            TotalStudentCount = TotalStudentCount + Item.StudentCount;
                        }
                        else
                        {
                            _unitOfWork.ExamCenterDetailRepository.Delete(PastRecord.Id);
                            _unitOfWork.Save();
                        }
                    }
                    else
                    {
                        if (Item.IsSelected)
                        {
                            PastRecord = new ExamCenterDetail
                            {
                                ExamCenterId = record.ExamCenterMaster.ExamCenterId,
                                CenterCollegeId = record.ExamCenterMaster.CenterCollegeId,
                                ExamCollegeId = Item.ExamCollegeId,
                                CenterCapacity = record.ExamCenterMaster.Capacity,
                                StudentCount = Item.StudentCount
                            };
                            _unitOfWork.ExamCenterDetailRepository.Create(PastRecord);
                            TotalStudentCount = TotalStudentCount + PastRecord.StudentCount;
                        }

                    }
                }
                if(TotalStudentCount <= record.ExamCenterMaster.Capacity)
                {
                    Scope.Complete();
                    Succeded = true;
                    Message = "College assigned to the center successfully";
                }
                
            }
        }
        #endregion
    }
}

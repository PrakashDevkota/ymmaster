﻿using MasterInterface.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MasterInterface.Model;
using MasterRepository.UnitOfWork;
using MasterRepository;
using System.Web.Mvc;
using System.Transactions;

namespace IOEInterface.Implementation
{
    public class ExamService : IExamService
    {
        #region Constructor
        private readonly IUnitOfWork _unitOfWork;
        public ExamService(IUnitOfWork unitOfWork = null)
        {
            _unitOfWork = unitOfWork ?? new UnitOfWork();
        }


        #endregion
        #region ExamDeclare
        public List<ExamDeclareModel> GetAllExamDeclares()
        {
            List<ExamDeclare> Record = _unitOfWork.ExamDeclareRepository.All().ToList();
            List<ExamDeclareModel> ModelRecord = Mapper.ExamDeclareMapper.ExamDeclareDbToModelList(Record);
            return ModelRecord;
        }
        public IEnumerable<SelectListItem> GetAllExamDeclareSelectList()
        {
            List<SelectListItem> Record = new List<SelectListItem>();
            List<ExamDeclare> Domain = _unitOfWork.ExamDeclareRepository.All().ToList();
            foreach (ExamDeclare Item in Domain)
            {
                SelectListItem ListItem = new SelectListItem();
                ListItem.Value = Item.Id.ToString();
                ListItem.Text = Item.Name;
                Record.Add(ListItem);
            }

            return Record;
         }


        private ExamDeclareModel GetFirstExamDeclare(string ExamType)
        {
            ExamDeclare Record = _unitOfWork.ExamDeclareRepository.Get(x=>x.ExamType == ExamType).FirstOrDefault();
            ExamDeclareModel ModelRecord = Mapper.ExamDeclareMapper.ExamDeclareDbToModel(Record);
            return ModelRecord;
        }

        public List<ExamDeclareModel> GetActiveRegularExamDeclareList()
        {
            List<ExamDeclare> Record = _unitOfWork.ExamDeclareRepository.Get(x => x.IsActive == true && x.RegistrationStartDate <= DateTime.Now && x.ExtenededSecondDate >= DateTime.Now && x.ExamType=="R" ).ToList();
            List<ExamDeclareModel> ModelRecord = Mapper.ExamDeclareMapper.ExamDeclareDbToModelList(Record);
            return ModelRecord;
        }
        public List<ExamDeclareModel> GetActiveBackExamDeclareList()
        {
            List<ExamDeclare> Record = _unitOfWork.ExamDeclareRepository.Get(x => x.IsActive == true && x.RegistrationStartDate <= DateTime.Now && x.ExtenededSecondDate >= DateTime.Now && x.ExamType =="B").ToList();
            List<ExamDeclareModel> ModelRecord = Mapper.ExamDeclareMapper.ExamDeclareDbToModelList(Record);
            return ModelRecord;
        }

        public void AddExamDeclare(ExamDeclareModel ExamDeclareModel)
        {
            ExamDeclare Record = Mapper.ExamDeclareMapper.ExamDeclareModelToDb(ExamDeclareModel);
            _unitOfWork.ExamDeclareRepository.Create(Record);
        }

        public void UpdateExamDeclare(ExamDeclareModel ExamDeclareModel)
        {
            ExamDeclare Record = Mapper.ExamDeclareMapper.ExamDeclareModelToDb(ExamDeclareModel);
            _unitOfWork.ExamDeclareRepository.Update(Record);
        }

        public ExamDeclareModel GetOneExamDeclare(int id)
        {
            ExamDeclare Record = _unitOfWork.ExamDeclareRepository.GetById(id);
            ExamDeclareModel ReturnRecord = Mapper.ExamDeclareMapper.ExamDeclareDbToModel(Record);
            return ReturnRecord;
        }
        public List<SelectListItem> GetExamDeclareSelectList()
        {
            List<SelectListItem> Record = new List<SelectListItem>();
            List<ExamDeclare> Domain = _unitOfWork.ExamDeclareRepository.Get(x => x.IsActive == true).ToList();
            foreach(ExamDeclare Item in Domain)
            {
                SelectListItem ListItem = new SelectListItem();
                ListItem.Value = Item.Id.ToString();
                ListItem.Text = Item.Name;
                Record.Add(ListItem);
            }
            return Record;
        }
        #endregion
        #region ExamRegistrationMaster
        public List<ExamRegistrationMasterModel> GetAllExamRegistrationMasters()
        {
            List<ExamRegistrationMaster> Record = _unitOfWork.ExamRegistrationMasterRepository.All().ToList();
            List<ExamRegistrationMasterModel> ModelRecord = Mapper.ExamRegistrationMasterMapper.ExamRegistrationMasterDbToModelList(Record);
            return ModelRecord;
        }


        public List<ExamRegistrationMasterModel> GetUniqueCollegeExamRegistrationMaster()
        {
            List<ExamRegistrationMaster> Record = _unitOfWork.ExamRegistrationMasterRepository.ExecuteSPwithParameterForList("GetExamRegistrationDistinctInstitutes");
            foreach (ExamRegistrationMaster Item in Record)
            {
                Item.ExamDeclare = _unitOfWork.ExamDeclareRepository.GetById(Item.ExamDeclareId);
                Item.CourseInstituteBody = _unitOfWork.CourseInstituteBodyRepository.GetById(Item.InstituteBodyId);
                Item.Student = _unitOfWork.StudentRepository.GetById(Item.StudentId);
                Item.CourseDegree = _unitOfWork.CourseDegreeRepository.GetById(Item.CourseDegreeId);
            }
            List<ExamRegistrationMasterModel> ModelRecord = Mapper.ExamRegistrationMasterMapper.ExamRegistrationMasterDbToModelList(Record);
           
            return ModelRecord;
        }

        //Add Regular exam record to master table, exam detail table and  fee structure detail table
        public void AddRegularExamRegistrationMasterAndExamRegistrationDetail(ExamRegistrationMasterModel ExamRegistrationMasterModel,int id)
        {
            ExamRegistrationMasterModel Record = new ExamRegistrationMasterModel();
            Record = this.GetRegularExamForm(id, ExamRegistrationMasterModel.ExamDeclareId);
             using (TransactionScope Scope = new TransactionScope())
            {
                // insert record into exam master table
                ExamRegistrationMaster ExamRegistrationMasterRecord = new ExamRegistrationMaster
                {
                    ExamDeclareId = Record.ExamDeclareId,
                    StudentId = Record.StudentId,
                    InstituteBodyId = Record.InstituteBodyId,
                    CourseDegreeId = Record.CourseDegreeId,
                    CampusRollNumber = Record.CampusRollNumber,
                    ExamRollNumber = Record.ExamRollNumber,
                    ExaminationYear = Convert.ToInt32(Record.ExaminationYear),
                    ExamYearCount = Convert.ToInt32(Record.ExamYearCount),
                    ExamPartCount = Convert.ToInt32(Record.ExamPartCount),
                    BankVoucherNumber = ExamRegistrationMasterModel.BankVoucherNumber,
                    StudentImage = ExamRegistrationMasterModel.StudentImage,
                    CreatedBy = ExamRegistrationMasterModel.CreatedBy,
                    CreatedOn = ExamRegistrationMasterModel.CreatedOn
                };
                ExamRegistrationMasterRecord = _unitOfWork.ExamRegistrationMasterRepository.Create(ExamRegistrationMasterRecord);
            
                // insert selected back record into exam detail table
               if (Record.DetailCompularyModel.Count > 0)
                {
                    foreach (var Row in Record.DetailCompularyModel)
                    {
                        ExamRegistrationDetail ExamRegistrationDetailRecord = new ExamRegistrationDetail
                        {
                            RegistrationMasterId = ExamRegistrationMasterRecord.Id,
                            CourseId = Row.CourseId,
                            ExamType = Row.ExamType,
                            Remarks = Row.Remarks

                        };
                        _unitOfWork.ExamRegistrationDetailRepository.Create(ExamRegistrationDetailRecord);
                    }
                }
                if (Record.DetailOptionalModel != null)
                {
                    foreach (var Row in Record.DetailOptionalModel)
                    {
                        if (Row.IsSelected)
                        {
                            ExamRegistrationDetail ExamRegistrationDetailRecord = new ExamRegistrationDetail
                            {
                                RegistrationMasterId = ExamRegistrationMasterRecord.Id,
                                CourseId = Row.CourseId,
                                ExamType = Row.ExamType,
                                Remarks = Row.Remarks

                            };
                            _unitOfWork.ExamRegistrationDetailRepository.Create(ExamRegistrationDetailRecord);
                        }


                    }
                }

                // insert fee record into exam register fee detail table
                ExamRegisterFeeDetail ExamRegisterFeeDetailRecord = new ExamRegisterFeeDetail
                {
                    ExamRegisterMasterId = ExamRegistrationMasterRecord.Id,
                    ExamDeclareId = ExamRegistrationMasterRecord.ExamDeclareId,
                    RegularAffiliated = Record.RegularAffiliated,
                    RegualarNonAffiliated = Record.RegualarNonAffiliated,
                    BackAffiliated = Record.BackAffiliated,
                    BackNonAffiliated = Record.BackNonAffiliated,
                    ExamFormFee = Record.ExamFormFee,
                    ExamCenterFee = Record.ExamCenterFee,
                    Type = Record.Type,
                    LateFee=Record.ExamLateFee
                };
            _unitOfWork.ExamRegisterFeeDetailRepository.Create(ExamRegisterFeeDetailRecord);
                Scope.Complete();
            }


        }

      

        public void AddBackExamRegistrationMasterAndExamRegistrationDetail(ExamRegistrationMasterModel ExamRegistrationMasterModel, int id)
        {
            ExamRegistrationMasterModel Record = new ExamRegistrationMasterModel();
            Record = this.GetBackExamForm(id, ExamRegistrationMasterModel.ExamDeclareId);
            Record.ExamDeclare = Mapper.ExamDeclareMapper.ExamDeclareDbToModel(_unitOfWork.ExamDeclareRepository.GetById(Record.ExamDeclareId));
            Record.DetailCompularyModel = ExamRegistrationMasterModel.DetailCompularyModel;
            int YearCount = 0;
            int PartCount = 0;
            using (TransactionScope Scope = new TransactionScope())
            {
                foreach (ExamRegistrationDetailModel Row in Record.DetailCompularyModel)
                {
                    if (Row.IsSelected)
                    {
                        if (Row.YearCount != YearCount || Row.PartCount != PartCount)
                        {
                            ExamRegistrationMaster ExamRegistrationMasterRecord = new ExamRegistrationMaster
                            {
                                ExamDeclareId = Record.ExamDeclareId,
                                StudentId = Record.StudentId,
                                InstituteBodyId = Record.InstituteBodyId,
                                CourseDegreeId = Record.CourseDegreeId,
                                CampusRollNumber = Record.CampusRollNumber,
                                ExamRollNumber = 0,
                                ExaminationYear = Convert.ToInt32(Record.ExamDeclare.ExamYear),
                                ExamYearCount = Row.YearCount,
                                ExamPartCount = Row.PartCount,
                                BankVoucherNumber = ExamRegistrationMasterModel.BankVoucherNumber,
                                StudentImage = ExamRegistrationMasterModel.StudentImage,
                                CreatedBy = ExamRegistrationMasterModel.CreatedBy,
                                CreatedOn = ExamRegistrationMasterModel.CreatedOn
                            };
                            ExamRegistrationMasterRecord = _unitOfWork.ExamRegistrationMasterRepository.Create(ExamRegistrationMasterRecord);
                            YearCount = Row.YearCount;
                            PartCount = Row.PartCount;
                            ExamRegisterFeeDetail ExamRegisterFeeDetailRecord = new ExamRegisterFeeDetail
                            {
                                ExamRegisterMasterId = ExamRegistrationMasterRecord.Id,
                                ExamDeclareId = Record.ExamDeclareId,
                                RegularAffiliated = Record.RegularAffiliated,
                                RegualarNonAffiliated = Record.RegualarNonAffiliated,
                                BackAffiliated = Record.BackAffiliated,
                                BackNonAffiliated = Record.BackNonAffiliated,
                                ExamFormFee = Record.ExamFormFee,
                                ExamCenterFee = Record.ExamCenterFee,
                                Type = Record.Type,
                                LateFee = Record.ExamLateFee
                            };
                            _unitOfWork.ExamRegisterFeeDetailRepository.Create(ExamRegisterFeeDetailRecord);
                        }
                        int ExamRegisterationMasterId = _unitOfWork.ExamRegistrationMasterRepository.Get(x => x.StudentId == Record.StudentId && x.ExamYearCount == Row.YearCount && x.ExamPartCount == Row.PartCount && x.InstituteBodyId == Record.InstituteBodyId).Select(x => x.Id).FirstOrDefault();
                        ExamRegistrationDetail ExamRegistrationDetailRecord = new ExamRegistrationDetail
                        {
                            RegistrationMasterId = ExamRegisterationMasterId,
                            CourseId = Row.CourseId,
                            ExamType = Row.ExamType,
                            Remarks = Row.Remarks

                        };
                        _unitOfWork.ExamRegistrationDetailRepository.Create(ExamRegistrationDetailRecord);

                    }
                }
                Scope.Complete();
            }
        }


        public ExamRegistrationMasterModel GetRegularExamForm(int LoginId, int ExamDeclareId)
        {
            ExamRegistrationMasterModel Record = new ExamRegistrationMasterModel();
            List<ExamRegistrationDetailModel> ExamCompulsoryRegistrationDetailListRecord = new List<ExamRegistrationDetailModel>();
            List<ExamRegistrationDetailModel> ExamOptionalRegistrationDetailListRecord = new List<ExamRegistrationDetailModel>();
            LoginStudent LoginStudentRecord = _unitOfWork.LoginStudentRepository.GetById(LoginId);
            Student StudentRecord = _unitOfWork.StudentRepository.GetById(LoginStudentRecord.StudentId);
            ExamDeclare ExamDeclareRecord = _unitOfWork.ExamDeclareRepository.GetById(ExamDeclareId);
            decimal ExamLateFee = GetLateFee(ExamDeclareRecord.RegistrationStartDate, ExamDeclareRecord.RegistrationEndDate, ExamDeclareRecord.ExtendedFirstDate, ExamDeclareRecord.ExtenededSecondDate, ExamDeclareRecord.ExamType);

            if (StudentRecord.CompleteFlag == false)
            {
                if (StudentRecord.CourseDegree.CourseDegreeLevel.DegreeLevelCode == "B")
                {
                    if (StudentRecord.Affiliated == "A")
                    {
                        ExamFeeStructure FeestructureRecord = _unitOfWork.ExamFeeStructureRepository.Get(x => x.Type == "B").FirstOrDefault();
                        Record.RegualarNonAffiliated = 0;
                        Record.RegularAffiliated = FeestructureRecord.RegularAffiliated;
                        Record.BackAffiliated = 0;
                        Record.BackNonAffiliated = 0;
                        Record.ExamCenterFee = FeestructureRecord.ExamCenterFee;
                        Record.ExamFormFee = FeestructureRecord.ExamFormFee;
                        Record.Type = FeestructureRecord.Type;
                        Record.ExamLateFee = ExamLateFee;
                    }
                    else
                    {
                        ExamFeeStructure FeestructureRecord = _unitOfWork.ExamFeeStructureRepository.Get(x => x.Type == "B").FirstOrDefault();
                        Record.RegualarNonAffiliated = FeestructureRecord.RegualarNonAffiliated;
                        Record.RegularAffiliated = 0;
                        Record.BackAffiliated = 0;
                        Record.BackNonAffiliated = 0;
                        Record.ExamCenterFee = FeestructureRecord.ExamCenterFee;
                        Record.ExamFormFee = FeestructureRecord.ExamFormFee;
                        Record.Type = FeestructureRecord.Type;
                        Record.ExamLateFee = ExamLateFee;
                    }

                }
                else if (StudentRecord.CourseDegree.CourseDegreeLevel.DegreeLevelCode == "M")
                {
                    if (StudentRecord.Affiliated == "A")
                    {
                        ExamFeeStructure FeestructureRecord = _unitOfWork.ExamFeeStructureRepository.Get(x => x.Type == "M").FirstOrDefault();
                        Record.RegualarNonAffiliated = 0;
                        Record.RegularAffiliated = FeestructureRecord.RegularAffiliated;
                        Record.BackAffiliated = 0;
                        Record.BackNonAffiliated = 0;
                        Record.ExamCenterFee = FeestructureRecord.ExamCenterFee;
                        Record.ExamFormFee = FeestructureRecord.ExamFormFee;
                        Record.Type = FeestructureRecord.Type;
                        Record.ExamLateFee = ExamLateFee;
                    }
                    else
                    {
                        ExamFeeStructure FeestructureRecord = _unitOfWork.ExamFeeStructureRepository.Get(x => x.Type == "M").FirstOrDefault();
                        Record.RegualarNonAffiliated = FeestructureRecord.RegualarNonAffiliated;
                        Record.RegularAffiliated = 0;
                        Record.BackAffiliated = 0;
                        Record.BackNonAffiliated = 0;
                        Record.ExamCenterFee = FeestructureRecord.ExamCenterFee;
                        Record.ExamFormFee = FeestructureRecord.ExamFormFee;
                        Record.Type = FeestructureRecord.Type;
                        Record.ExamLateFee = ExamLateFee;
                    }

                }

                List<Course> CompulsoryCourseRecord = _unitOfWork.CourseRepository.Get(x => x.DegreeId == StudentRecord.DegreeId && x.AcademicPart == StudentRecord.CurrentAcedemicPart && x.AcademicYear <= StudentRecord.CurrentAcedemicYear && x.FOSId == StudentRecord.FOSId && x.CourseDegree.LevelId == StudentRecord.CourseDegree.LevelId && x.Compulsory == true && x.AssessmentType == "C").ToList();
                if (CompulsoryCourseRecord != null)
                {
                    foreach (Course Row in CompulsoryCourseRecord)
                    {
                        ExamRegistrationDetailModel ExamRegistrationCompulsaryDetailRecord = new ExamRegistrationDetailModel
                        {
                            CourseId = Row.Id,
                            ExamType = ExamDeclareRecord.ExamType,
                            CourseName = Row.Name,
                            CourseCode = Row.Code
                        };
                        ExamCompulsoryRegistrationDetailListRecord.Add(ExamRegistrationCompulsaryDetailRecord);
                    }

                }

                List<Course> OptionalCourseRecord = _unitOfWork.CourseRepository.Get(x => x.DegreeId == StudentRecord.DegreeId && x.AcademicPart == StudentRecord.CurrentAcedemicPart && x.AcademicYear <= StudentRecord.CurrentAcedemicYear && x.FOSId == StudentRecord.FOSId && x.CourseDegree.LevelId == StudentRecord.CourseDegree.LevelId && x.Compulsory == false && x.AssessmentType == "O").ToList();
                if (OptionalCourseRecord != null)
                {
                    foreach (Course Row in OptionalCourseRecord)
                    {
                        ExamRegistrationDetailModel ExamRegistrationOptionalDetailRecord = new ExamRegistrationDetailModel
                        {
                            CourseId = Row.Id,
                            ExamType = ExamDeclareRecord.ExamType,
                            CourseName = Row.Name,
                            CourseCode = Row.Code
                        };
                        ExamOptionalRegistrationDetailListRecord.Add(ExamRegistrationOptionalDetailRecord);
                    }
                }
                if (CompulsoryCourseRecord != null)
                {
                    Record.DetailCompularyModel = ExamCompulsoryRegistrationDetailListRecord;
                    Record.DetailOptionalModel = ExamOptionalRegistrationDetailListRecord;
                    Record.StudentId = StudentRecord.Id;
                    Record.Affiliated = StudentRecord.Affiliated;
                    Record.InstituteBodyId = StudentRecord.InstituteBodyId;
                    Record.CourseDegreeId = StudentRecord.DegreeId;
                    Record.ExamDeclareId = ExamDeclareId;
                    Record.CampusRollNumber = StudentRecord.CampusRollNumber;
                    Record.ExaminationYear = ExamDeclareRecord.ExamYear;
                    Record.ExamRollNumber = 0;
                    Record.ExamYearCount = StudentRecord.CurrentAcedemicYear;
                    Record.ExamPartCount = StudentRecord.CurrentAcedemicPart;
                    Record.InstituteBodyName = StudentRecord.CourseInstituteBody.InstituteBodyName;
                    Record.CourseDegreeName = StudentRecord.CourseDegree.DegreeName;
                    Record.CreatedBy = LoginId;
                    Record.CreatedOn = DateTime.Now;
                }


            }
            if (StudentRecord.CompleteFlag == true)
            {
                Record.CompleteFlag = true;
            }
            return Record;
        }


        private decimal GetLateFee(DateTime StartDate, DateTime EndDate, DateTime FirstExtendedDate, DateTime SecondExtendedDate, string ExamType)
        {
            decimal ReturnValue = 0;

            if (DateTime.Now.Date > EndDate.Date && DateTime.Now.Date <= FirstExtendedDate.Date)
            {
                ExamDeclareModel Record = this.GetFirstExamDeclare(ExamType);
                ReturnValue = Record.ExtendedFirstDateCharge;
            }
            if (DateTime.Now.Date > FirstExtendedDate.Date && DateTime.Now.Date <= SecondExtendedDate.Date)
            {
                ExamDeclareModel Record = this.GetFirstExamDeclare(ExamType);
                ReturnValue = Record.ExtendedSecondDateCharge;
            }
            return ReturnValue;
        }

        public ExamRegistrationMasterModel GetBackExamForm(int LoginId, int ExamDeclareId)
        {
            ExamRegistrationMasterModel Record = new ExamRegistrationMasterModel();
            LoginStudent LoginStudentRecord = _unitOfWork.LoginStudentRepository.GetById(LoginId);
            Student StudentRecord = _unitOfWork.StudentRepository.GetById(LoginStudentRecord.StudentId);
            ExamDeclare ExamDeclareRecord = _unitOfWork.ExamDeclareRepository.GetById(ExamDeclareId);
            decimal ExamLateFee = GetLateFee(ExamDeclareRecord.RegistrationStartDate, ExamDeclareRecord.RegistrationEndDate, ExamDeclareRecord.ExtendedFirstDate, ExamDeclareRecord.ExtenededSecondDate, ExamDeclareRecord.ExamType);
            if (StudentRecord.CompleteFlag == false)
            {
                if (StudentRecord.CourseDegree.CourseDegreeLevel.DegreeLevelCode == "B")
                {
                    if (StudentRecord.Affiliated == "A")
                    {
                        ExamFeeStructure FeestructureRecord = _unitOfWork.ExamFeeStructureRepository.Get(x => x.Type == "B").FirstOrDefault();
                        Record.RegualarNonAffiliated = 0;
                        Record.RegularAffiliated = 0;
                        Record.BackAffiliated = FeestructureRecord.BackAffiliated;
                        Record.BackNonAffiliated = 0;
                        Record.ExamCenterFee = FeestructureRecord.ExamCenterFee;
                        Record.ExamFormFee = FeestructureRecord.ExamFormFee;
                        Record.Type = FeestructureRecord.Type;
                        Record.ExamLateFee = ExamLateFee;
                    }
                    else
                    {
                        ExamFeeStructure FeestructureRecord = _unitOfWork.ExamFeeStructureRepository.Get(x => x.Type == "B").FirstOrDefault();
                        Record.RegualarNonAffiliated = 0;
                        Record.RegularAffiliated = 0;
                        Record.BackAffiliated = 0;
                        Record.BackNonAffiliated = FeestructureRecord.BackNonAffiliated;
                        Record.ExamCenterFee = FeestructureRecord.ExamCenterFee;
                        Record.ExamFormFee = FeestructureRecord.ExamFormFee;
                        Record.Type = FeestructureRecord.Type;
                        Record.ExamLateFee = ExamLateFee;
                    }

                }
                else if (StudentRecord.CourseDegree.CourseDegreeLevel.DegreeLevelCode == "M")
                {
                    if (StudentRecord.Affiliated == "A")
                    {
                        ExamFeeStructure FeestructureRecord = _unitOfWork.ExamFeeStructureRepository.Get(x => x.Type == "M").FirstOrDefault();
                        Record.RegualarNonAffiliated = 0;
                        Record.RegularAffiliated = 0;
                        Record.BackAffiliated = FeestructureRecord.BackAffiliated;
                        Record.BackNonAffiliated = 0;
                        Record.ExamCenterFee = FeestructureRecord.ExamCenterFee;
                        Record.ExamFormFee = FeestructureRecord.ExamFormFee;
                        Record.Type = FeestructureRecord.Type;
                        Record.ExamLateFee = ExamLateFee;

                    }
                    else
                    {
                        ExamFeeStructure FeestructureRecord = _unitOfWork.ExamFeeStructureRepository.Get(x => x.Type == "M").FirstOrDefault();
                        Record.RegualarNonAffiliated = 0;
                        Record.RegularAffiliated = 0;
                        Record.BackAffiliated = 0;
                        Record.BackNonAffiliated = FeestructureRecord.BackNonAffiliated;
                        Record.ExamCenterFee = FeestructureRecord.ExamCenterFee;
                        Record.ExamFormFee = FeestructureRecord.ExamFormFee;
                        Record.Type = FeestructureRecord.Type;
                        Record.ExamLateFee = ExamLateFee;
                    }
                }
                if (StudentRecord.BackPaperFlag)
                {
                    List<int> ExamBackPaperRecord = _unitOfWork.ExamBackPaperRepository.Get(x => x.StudentId == StudentRecord.Id).Select(x => x.CourseId).ToList();
                    List<ExamRegistrationDetailModel> ExamRegistrationDetailListRecord = new List<ExamRegistrationDetailModel>();
                    List<Course> CourseRecord = new List<Course>();
                    if (StudentRecord.BackPaperFlag)
                    {
                        if (StudentRecord.BarrierFlag)
                        {
                            CourseRecord = _unitOfWork.CourseRepository.Get(x => ExamBackPaperRecord.Contains(x.Id)).ToList();
                        }
                        else if (ExamDeclareRecord.EvenOdd == "E" && StudentRecord.CurrentAcedemicPart == 2)
                        {
                             CourseRecord = _unitOfWork.CourseRepository.Get(x => ExamBackPaperRecord.Contains(x.Id)).Where(x => x.AcademicPart == 2).ToList();
                        }
                        else if (ExamDeclareRecord.EvenOdd == "O" && StudentRecord.CurrentAcedemicPart == 1)
                        {
                            CourseRecord = _unitOfWork.CourseRepository.Get(x => ExamBackPaperRecord.Contains(x.Id)).Where(x => x.AcademicPart == 1).ToList();

                        }

                    }
                    if (CourseRecord.Count != 0)
                    {
                        foreach (Course Row in CourseRecord)
                        {
                            ExamRegistrationDetailModel ExamRegistrationDetailRecord = new ExamRegistrationDetailModel
                            {
                                CourseId = Row.Id,
                                ExamType = ExamDeclareRecord.ExamType,
                                CourseName = Row.Name,
                                CourseCode = Row.Code,
                                YearCount=Convert.ToInt32(Row.AcademicYear),
                                PartCount=Convert.ToInt32(Row.AcademicPart)
                            };
                            ExamRegistrationDetailListRecord.Add(ExamRegistrationDetailRecord);
                        }
                        Record.DetailCompularyModel = ExamRegistrationDetailListRecord.OrderBy(x=>x.YearCount).ThenBy(x=>x.PartCount).ToList();
                        Record.StudentId = StudentRecord.Id;
                        Record.Affiliated = StudentRecord.Affiliated;
                        Record.InstituteBodyId = StudentRecord.InstituteBodyId;
                        Record.CourseDegreeId = StudentRecord.DegreeId;
                        Record.ExamDeclareId = ExamDeclareId;
                        Record.CampusRollNumber = StudentRecord.CampusRollNumber;
                        Record.ExaminationYear = ExamDeclareRecord.ExamYear;
                        Record.ExamRollNumber = 0;
                        Record.ExamYearCount = StudentRecord.CurrentAcedemicYear;
                        Record.ExamPartCount = StudentRecord.CurrentAcedemicPart;
                        Record.InstituteBodyName = StudentRecord.CourseInstituteBody.InstituteBodyName;
                        Record.CourseDegreeName = StudentRecord.CourseDegree.DegreeName;
                        Record.CreatedBy = LoginId;
                        Record.CreatedOn = DateTime.Now;
                    }
                }
            }
            return Record;
        }

        public void UpdateExamRegistrationMaster(ExamRegistrationMasterModel ExamRegistrationMasterModel)
        {
            ExamRegistrationMaster Record = Mapper.ExamRegistrationMasterMapper.ExamRegistrationMasterModelToDb(ExamRegistrationMasterModel);
            _unitOfWork.ExamRegistrationMasterRepository.Update(Record);
        }

        public ExamRegistrationMasterModel GetOneExamRegistrationMaster(int id)
        {
            ExamRegistrationMaster Record = _unitOfWork.ExamRegistrationMasterRepository.GetById(id);
            ExamRegistrationMasterModel ReturnRecord = Mapper.ExamRegistrationMasterMapper.ExamRegistrationMasterDbToModel(Record);
            return ReturnRecord;
        }


        #endregion
       


        #region Exam Fee Structure
        public List<ExamFeeStructureModel> GetAllExamFeeStructures()
        {
            List<ExamFeeStructure> Record = _unitOfWork.ExamFeeStructureRepository.All().ToList();
            List<ExamFeeStructureModel> ModelRecord = Mapper.ExamFeeStructureMapper.ExamFeeStructureDbToModelList(Record);
            return ModelRecord;
        }
       

        public void AddExamFeeStructure(ExamFeeStructureModel ExamFeeStructureModel)
        {
            ExamFeeStructure Record = Mapper.ExamFeeStructureMapper.ExamFeeStructureModelToDb(ExamFeeStructureModel);
            _unitOfWork.ExamFeeStructureRepository.Create(Record);
        }

        public void UpdateExamFeeStructure(ExamFeeStructureModel ExamFeeStructureModel)
        {
            ExamFeeStructure Record = Mapper.ExamFeeStructureMapper.ExamFeeStructureModelToDb(ExamFeeStructureModel);
            _unitOfWork.ExamFeeStructureRepository.Update(Record);
        }

        public ExamFeeStructureModel GetOneExamFeeStructure(int id)
        {
            ExamFeeStructure Record = _unitOfWork.ExamFeeStructureRepository.GetById(id);
            ExamFeeStructureModel ReturnRecord = Mapper.ExamFeeStructureMapper.ExamFeeStructureDbToModel(Record);
            return ReturnRecord;
        }


        #endregion

        #region Exam Back Paper
        public List<ExamBackPaperModel> GetAllExamBackPapers()
        {
            List<ExamBackPaper> Record = _unitOfWork.ExamBackPaperRepository.All().ToList();
            List<ExamBackPaperModel> ModelRecord = Mapper.ExamBackPaperMapper.ExamBackPaperDbToModelList(Record);
            return ModelRecord;
        }
        public List<ExamBackPaperModel> GetAllExamBackPaperByInstituteBodyId(int id)
        {
            List<ExamBackPaper> Record = _unitOfWork.ExamBackPaperRepository.Get(x => x.InstituteId == id).ToList();
            List<ExamBackPaperModel> ModelRecord = Mapper.ExamBackPaperMapper.ExamBackPaperDbToModelList(Record);
            return ModelRecord;
                      
        }

        public void AddExamBackPaper(ExamBackPaperModel ExamBackPaperModel)
        {
            ExamBackPaper Record = Mapper.ExamBackPaperMapper.ExamBackPaperModelToDb(ExamBackPaperModel);
            _unitOfWork.ExamBackPaperRepository.Create(Record);
        }

        public void UpdateExamBackPaper(ExamBackPaperModel ExamBackPaperModel)
        {
            ExamBackPaper Record = Mapper.ExamBackPaperMapper.ExamBackPaperModelToDb(ExamBackPaperModel);
            _unitOfWork.ExamBackPaperRepository.Update(Record);
        }

        public ExamBackPaperModel GetOneExamBackPaper(int id)
        {
            ExamBackPaper Record = _unitOfWork.ExamBackPaperRepository.GetById(id);
            ExamBackPaperModel ReturnRecord = Mapper.ExamBackPaperMapper.ExamBackPaperDbToModel(Record);
            return ReturnRecord;
        }

      
       public  IEnumerable<SelectListItem> GetExamBackPaperStudentSelectList()
        {
            IStudentService _studentservice = new StudentService();
          List <StudentModel> Record = _studentservice.GetAllBackPaperStudentList();
            List<SelectListItem> ReturnRecord = new List<SelectListItem>();
            foreach (var row in Record)
            {
                ReturnRecord.Add(new SelectListItem
                {
                    Text = row.FullName,
                    Value = row.Id.ToString()
                });
            }
            return ReturnRecord;
        }

        #endregion

        public bool ValidateIfRegistered(int Id,int StudentId)
        {
          ExamRegistrationMaster Record= _unitOfWork.ExamRegistrationMasterRepository.Get(x => x.ExamDeclareId == Id && x.StudentId == StudentId).FirstOrDefault();
            if (Record!=null)
            {
                return false;
            }
            return true;
        }
    }
}

﻿using MasterInterface.Interface;
using MasterRepository.UnitOfWork;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MasterInterface.Model;
using MasterRepository;
using MasterInterface.Helper;
using System.Security.Cryptography;

namespace MasterInterface.Implementation
{
    public class LoginService : ILoginService
    {
        #region Constructor
        private readonly IUnitOfWork _unitOfWork;
        public LoginService(IUnitOfWork unitOfWork = null)
        {
            _unitOfWork = unitOfWork ?? new UnitOfWork();
        }

        #endregion

        #region Login
        public LoginModel GetLoginDetails(LoginModel Record)
        {
            LoginModel ReturnRecord = new LoginModel();
            UserDetail loginRecord = _unitOfWork.UserDetailRepository.Get(x => x.Username.ToLower() == Record.UserName.ToLower()).FirstOrDefault();
            if (loginRecord != null)
            {
                using (MD5 md5Hash = MD5.Create())
                {
                    if (HashPassword.VerifyMd5Hash(md5Hash, Record.Password, loginRecord.Password))
                    {
                        loginRecord = _unitOfWork.UserDetailRepository.Get(x => x.Username == Record.UserName && x.Password == loginRecord.Password).FirstOrDefault();
                        if (loginRecord != null)
                        {
                            ReturnRecord = Mapper.LoginMapper.LoginSuperAdminDbToModel(loginRecord);
                            return ReturnRecord;
                        }
                    }

                }
            }
         

            return ReturnRecord;
        }
        #endregion
    }
}

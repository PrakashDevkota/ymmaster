﻿using MasterInterface.Helper;
using MasterInterface.Interface;
using MasterInterface.Model;
using IOERepository;
using MasterRepository.UnitOfWork;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace MasterInterface.Implementation
{
    public class CourseService : ICourseService
    {
        #region Constructor
        private readonly IUnitOfWork _unitOfWork;
        public CourseService(IUnitOfWork unitOfWork = null)
        {
            _unitOfWork = unitOfWork ?? new UnitOfWork();
        }


        #endregion

        #region course
        public List<CourseModel> GetAllCourses()
        {
            List<Course> Record = _unitOfWork.CourseRepository.All().ToList();
            List<CourseModel> ModelRecord= Mapper.CourseMapper.CourseDbToModelList(Record);
            return ModelRecord;
        }
        public IEnumerable<SelectListItem> GetParentCourses()
        {
            List<Course> Record = _unitOfWork.CourseRepository.All().ToList();
            List<SelectListItem> ReturnRecord = new List<SelectListItem>();
            foreach (var row in Record)
            {
                ReturnRecord.Add(new SelectListItem
                {
                    Text = row.Name,
                    Value = row.Id.ToString()
                });
            }
            return ReturnRecord;
        }
        public IEnumerable<SelectListItem> GetCourseFieldOfStudyListSelectList()
        {
            List<CourseFieldOfStudy> Record = _unitOfWork.CourseFieldOfStudyRepository.All().ToList();
            List<SelectListItem> ReturnRecord = new List<SelectListItem>();
            foreach (var row in Record)
            {
                ReturnRecord.Add(new SelectListItem
                {
                    Text = row.Name,
                    Value = row.Id.ToString()
                });
            }
            return ReturnRecord;
        }
        public List<SelectListItem> GetCourseDegreeSelectList()
        {
            List<CourseDegree> Record = _unitOfWork.CourseDegreeRepository.All().ToList();
            List<SelectListItem> ReturnRecord = new List<SelectListItem>();
            foreach (var row in Record)
            {
                ReturnRecord.Add(new SelectListItem
                {
                    Text = row.DegreeName,
                    Value = row.Id.ToString()
                });
            }
            return ReturnRecord;
        }
        public void AddCourse(CourseModel courseModel)
        {
            Course Record = Mapper.CourseMapper.CourseModelToDb(courseModel);
            _unitOfWork.CourseRepository.Create(Record);
        }
        public void UpdateCourse(CourseModel courseModel)
        {
            Course Record = Mapper.CourseMapper.CourseModelToDb(courseModel);
            _unitOfWork.CourseRepository.Update(Record);
        }
        public CourseModel GetOneCourse(int id)
        {
          Course Record = _unitOfWork.CourseRepository .GetById(id);
          CourseModel ReturnRecord=  Mapper.CourseMapper.CourseDbToModel(Record);
          return ReturnRecord;
        }
        public void DeleteCourse(int id)
        {
            _unitOfWork.CourseRepository.Delete(id);
        }
        public bool IsCourseCodeExists(string CourseCode)
        {
            Course Record = _unitOfWork.CourseRepository.Get(x => x.Code == CourseCode).FirstOrDefault();
            if (Record == null)
            {
                return false;
            }
            else
            {
                return true;
            }
        }
        public bool IsCourseNameExists(string CourseName)
        {
            Course Record = _unitOfWork.CourseRepository.Get(x=>x.Name == CourseName).FirstOrDefault();
            if (Record == null)
            {
                return false;
            }
            else
            {
                return true;
            }
        }
        #endregion

        #region course degree
        public void AddCourseDegree(CourseDegreeModel courseDegreeModel)
        {
            CourseDegree Record = Mapper.CourseDegreeMapper.CourseDegreeModelToDb(courseDegreeModel);
            _unitOfWork.CourseDegreeRepository.Create(Record);
        }

        public List<CourseDegreeModel> GetAllCourseDegrees()
        {
            List<CourseDegree> Record = _unitOfWork.CourseDegreeRepository.All().ToList();
            List<CourseDegreeModel> ModelRecord = Mapper.CourseDegreeMapper.CourseDegreeDbToModelList(Record);
            return ModelRecord;
        }

        public CourseDegreeModel GetOneCourseDegree(int id)
        {
            CourseDegree Record = _unitOfWork.CourseDegreeRepository.GetById(id);
            CourseDegreeModel ReturnRecord = Mapper.CourseDegreeMapper.CourseDegreeDbToModel(Record);
            return ReturnRecord;
        }

        public bool IsCourseDegreeCodeExists(string CourseDegreeCode)
        {
            CourseDegree Record = _unitOfWork.CourseDegreeRepository.Get(x => x.DegreeCode == CourseDegreeCode).FirstOrDefault();
            if (Record == null)
            {
                return false;
            }
            else
            {
                return true;
            }
        }

        public bool IsCourseDegreeNameExists(string CourseDegreeName)
        {
            CourseDegree Record = _unitOfWork.CourseDegreeRepository.Get(x => x.DegreeCode == CourseDegreeName).FirstOrDefault();
            if (Record == null)
            {
                return false;
            }
            else
            {
                return true;
            }
        }


        public void UpdateCourseDegree(CourseDegreeModel courseDegreeModel)
        {
            CourseDegree Record = Mapper.CourseDegreeMapper.CourseDegreeModelToDb(courseDegreeModel);
            _unitOfWork.CourseDegreeRepository.Update(Record);
        }
        #endregion

        #region course degree level 
        public void AddCourseDegreeLevel(CourseDegreeLevelModel courseDegreeLevelModel)
        {
            CourseDegreeLevel Record = Mapper.CourseDegreeLevelMapper.CourseDegreeLevelModelToDb(courseDegreeLevelModel);
            _unitOfWork.CourseDegreeLevelRepository.Create(Record);
        }

        public List<CourseDegreeLevelModel> GetAllCourseDegreeLevel()
        {
            List<CourseDegreeLevel> Record = _unitOfWork.CourseDegreeLevelRepository.All().ToList();
            List<CourseDegreeLevelModel> ModelRecord = Mapper.CourseDegreeLevelMapper.CourseDegreeLevelDbToModelList(Record);
            return ModelRecord;
        }

        public CourseDegreeLevelModel GetOneCourseDegreeLevel(int id)
        {
            CourseDegreeLevel Record = _unitOfWork.CourseDegreeLevelRepository.GetById(id);
            CourseDegreeLevelModel ReturnRecord = Mapper.CourseDegreeLevelMapper.CourseDegreeLevelDbToModel(Record);
            return ReturnRecord;
        }


       
       


        public bool IsCourseDegreeLevelCodeExists(string CourseDegreeLevelCode)
        {
            CourseDegreeLevel Record = _unitOfWork.CourseDegreeLevelRepository.Get(x => x.DegreeLevelCode == CourseDegreeLevelCode).FirstOrDefault();
            if (Record == null)
            {
                return false;
            }
            else
            {
                return true;
            }
        }

        public bool IsCourseDegreeLevelNameExists(string CourseDegreeLevelName)
        {
            CourseDegreeLevel Record = _unitOfWork.CourseDegreeLevelRepository.Get(x => x.DegreeLevelName == CourseDegreeLevelName).FirstOrDefault();
            if (Record == null)
            {
                return false;
            }
            else
            {
                return true;
            }
        }

       
        public void UpdateCourseDegreeLevel(CourseDegreeLevelModel courseDegreeLevelModel)
        {
            CourseDegreeLevel Record = Mapper.CourseDegreeLevelMapper.CourseDegreeLevelModelToDb(courseDegreeLevelModel);
            _unitOfWork.CourseDegreeLevelRepository.Update(Record);
        }

        public IEnumerable<SelectListItem> GetCourseDegreeLevel()
        {
            List<CourseDegreeLevel> Record = _unitOfWork.CourseDegreeLevelRepository.All().ToList();
            List<SelectListItem> ReturnRecord = new List<SelectListItem>();
            foreach (var row in Record)
            {
                ReturnRecord.Add(new SelectListItem
                {
                    Text = row.DegreeLevelName,
                    Value = row.Id.ToString()
                });
            }
            return ReturnRecord;
        }

        #endregion

        #region course field of study
        public List<CourseFieldOfStudyModel> GetAllCourseFieldOfStudy()
        {
            List<CourseFieldOfStudy> Record = _unitOfWork.CourseFieldOfStudyRepository.All().ToList();
            List<CourseFieldOfStudyModel> ModelRecord = Mapper.CourseFieldOfStudyMapper.CourseFieldOfStudyDbToModelList(Record);
            return ModelRecord;
        }

        public void AddCourseFieldOfStudy(CourseFieldOfStudyModel courseFieldOfStudyModel)
        {
            CourseFieldOfStudy Record = Mapper.CourseFieldOfStudyMapper.CourseFieldOfStudyModelToDb(courseFieldOfStudyModel);
            _unitOfWork.CourseFieldOfStudyRepository.Create(Record);
        }

        public CourseFieldOfStudyModel GetOneCourseFieldOfStudy(int id)
        {
            CourseFieldOfStudy Record = _unitOfWork.CourseFieldOfStudyRepository.GetById(id);
            CourseFieldOfStudyModel ReturnRecord = Mapper.CourseFieldOfStudyMapper.CourseFieldOfStudyDbToModel(Record);
            return ReturnRecord;
        }

        public bool IsCourseFieldOfStudyCodeExists(string Code)
        {
            CourseFieldOfStudy Record = _unitOfWork.CourseFieldOfStudyRepository.Get(x => x.Code == Code).FirstOrDefault();
            if (Record == null)
            {
                return false;
            }
            else
            {
                return true;
            }
        }

        public bool IsCourseFieldOfStudyNameExists(string Name)
        {
            CourseFieldOfStudy Record = _unitOfWork.CourseFieldOfStudyRepository.Get(x => x.Code == Name).FirstOrDefault();
            if (Record == null)
            {
                return false;
            }
            else
            {
                return true;
            }
        }

        public void UpdateCourseFieldOfStudy(CourseFieldOfStudyModel courseFieldOfStudyModel)
        {
            CourseFieldOfStudy Record = Mapper.CourseFieldOfStudyMapper.CourseFieldOfStudyModelToDb(courseFieldOfStudyModel);
            _unitOfWork.CourseFieldOfStudyRepository.Update(Record);
        }
        #endregion
        
        #region course institute location

        public List<CourseInstituteLocationModel> GetAllCourseInstituteLocation()
        {
            List<CourseInstituteLocation> Record = _unitOfWork.CourseInstituteLocationRepository.All().ToList();
            List<CourseInstituteLocationModel> ReturnRecord = Mapper.CourseInstituteLocationMapper.CourseInstituteLocationDbToModelList(Record);
            return ReturnRecord;
        }

        public void AddCourseInstituteLocation(CourseInstituteLocationModel courseInstituteLocationModel)
        {
            CourseInstituteLocation Record = Mapper.CourseInstituteLocationMapper.CourseInstituteLocationModelToDb(courseInstituteLocationModel);
            _unitOfWork.CourseInstituteLocationRepository.Create(Record);
        }

        public CourseInstituteLocationModel GetOneCourseInstituteLocation(int id)
        {
            CourseInstituteLocation Record= _unitOfWork.CourseInstituteLocationRepository.GetById(id);
            CourseInstituteLocationModel ReturnRecord=Mapper.CourseInstituteLocationMapper.CourseInstituteLocationDbToModel(Record);
            return ReturnRecord;
        }

        public void UpdateCourseInstituteLocation(CourseInstituteLocationModel courseInstituteLocationModel)
        {
            CourseInstituteLocation Record = Mapper.CourseInstituteLocationMapper.CourseInstituteLocationModelToDb(courseInstituteLocationModel);
            _unitOfWork.CourseInstituteLocationRepository.Update(Record);
        }

        public bool ICourseInstituteLocationCodeExists(string CourseInstituteLocationeCode)
        {
            CourseInstituteLocation Record = _unitOfWork.CourseInstituteLocationRepository.Get(x => x.LocationCode == CourseInstituteLocationeCode).FirstOrDefault();
            if (Record != null)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public bool IsCourseInstituteLocationNameExists(string CourseInstituteLocationName)
        {
        CourseInstituteLocation Record=_unitOfWork.CourseInstituteLocationRepository.Get(x => x.LocationName == CourseInstituteLocationName).FirstOrDefault();
            if (Record != null)
            {
                return true;
            }
            else
            {
                return false;
            }
        }




        #endregion
        
        #region course institute body
        public List<CourseInstituteBodyModel> GetAllCourseInstituteBody()
        {
            List<CourseInstituteBody> Record = _unitOfWork.CourseInstituteBodyRepository.All().ToList();
            List<CourseInstituteBodyModel> ModelRecord = Mapper.CourseInstituteBodyMapper.CourseInstituteBodyDbToModelList(Record);
            foreach(CourseInstituteBodyModel Item in ModelRecord)
            {
                LoginCollege LoginModelRecord = _unitOfWork.LoginCollegeRepository.Get(x => x.InstituteBodyId == Item.Id).FirstOrDefault(); 
                if(LoginModelRecord != null)
                {
                    Item.IsUserCreated = true;
                }
            }
            return ModelRecord;
        }

        public IEnumerable<SelectListItem> GetCourseInstituteLocation()
        {
            List<CourseInstituteLocation> Record = _unitOfWork.CourseInstituteLocationRepository.All().ToList();
            List<SelectListItem> ReturnRecord = new List<SelectListItem>();
            foreach (var row in Record)
            {
                ReturnRecord.Add(new SelectListItem
                {
                    Text = row.LocationName,
                    Value = row.Id.ToString()
                });
            }
            return ReturnRecord;
        }

        public List<SelectListItem> GetCourseInstituteBodySelectList()
        {
            List<CourseInstituteBody> Record = _unitOfWork.CourseInstituteBodyRepository.All().ToList();
            List<SelectListItem> ReturnRecord = new List<SelectListItem>();
            foreach (var row in Record)
            {
                ReturnRecord.Add(new SelectListItem
                {
                    Text = row.InstituteBodyName,
                    Value = row.Id.ToString()
                });
            }
            return ReturnRecord;


        }

        public void AddCourseInstituteBody(CourseInstituteBodyModel courseInstituteBodyModel)
        {
         CourseInstituteBody Record = Mapper.CourseInstituteBodyMapper.CourseInstituteBodyModelToDb(courseInstituteBodyModel);
            _unitOfWork.CourseInstituteBodyRepository.Create(Record);
        }

        public CourseInstituteBodyModel GetOneCourseInstituteBody(int id)
        {
            CourseInstituteBody Record = _unitOfWork.CourseInstituteBodyRepository.GetById(id);
            CourseInstituteBodyModel ReturnRecord = Mapper.CourseInstituteBodyMapper.CourseInstituteBodyDbToModel(Record);
            return ReturnRecord;
        }

        public void UpdateCourseInstituteBody(CourseInstituteBodyModel courseInstituteBodyModel)
        {
          CourseInstituteBody Record = Mapper.CourseInstituteBodyMapper.CourseInstituteBodyModelToDb(courseInstituteBodyModel);
          _unitOfWork.CourseInstituteBodyRepository.Update(Record);
        }

      
        public bool IsCourseInstituteBodyCodeExists(string CourseInstituteBodyCode)
        {
            CourseInstituteBody Record = _unitOfWork.CourseInstituteBodyRepository.Get(x => x.InstituteBodyCode == CourseInstituteBodyCode).FirstOrDefault();
            if (Record == null)
            {
                return false;
            }
            else
            {
                return true;
            }
        }

        public bool IsCourseInstituteBodyNameExists(string CourseInstituteBodyName)
        {
            CourseInstituteBody Record = _unitOfWork.CourseInstituteBodyRepository.Get(x => x.InstituteBodyName == CourseInstituteBodyName).FirstOrDefault();
            if (Record == null)
            {
                return false;
            }
            else
            {
                return true;
            }
        }


        #endregion

        #region course master
        public List<CourseMasterModel> GetAllCourseMasters()
        {
            List<CourseMaster> Record = _unitOfWork.CourseMasterRepository.All().ToList();
            List<CourseMasterModel> ModelRecord = Mapper.CourseMasterMapper.CourseMasterDbToModelList(Record);
            return ModelRecord;
        }

       
        

        public IEnumerable<SelectListItem> GetCourseFeildOfStudyList()
        {
            List<CourseFieldOfStudy> Record = _unitOfWork.CourseFieldOfStudyRepository.All().ToList();
            List<SelectListItem> ReturnRecord = new List<SelectListItem>();
            foreach (var row in Record)
            {
                ReturnRecord.Add(new SelectListItem
                {
                    Text = row.Name,
                    Value = row.Id.ToString()
                });
            }
            return ReturnRecord;
        }

        public IEnumerable<SelectListItem> GetCourseDegreeList()
        {
            List<CourseDegree> Record = _unitOfWork.CourseDegreeRepository.All().ToList();
            List<SelectListItem> ReturnRecord = new List<SelectListItem>();
            foreach (var row in Record)
            {
                ReturnRecord.Add(new SelectListItem
                {
                    Text = row.DegreeName,
                    Value = row.Id.ToString()
                });
            }
            return ReturnRecord;
        }

        public IEnumerable<SelectListItem> GetCourseInstituteBodyTypeList()
        {
            return StaticSelectlist.GetBodyTypeSelectList();
        }


        public void AddCourseMaster(CourseMasterModel CourseMasterModel)
        {
            CourseMaster Record = Mapper.CourseMasterMapper.CourseMasterModelToDb(CourseMasterModel);
            _unitOfWork.CourseMasterRepository.Create(Record);
        }

        public void UpdateCourseMaster(CourseMasterModel CourseMasterModel)
        {
            CourseMaster Record = Mapper.CourseMasterMapper.CourseMasterModelToDb(CourseMasterModel);
            _unitOfWork.CourseMasterRepository.Update(Record);
        }

        public CourseMasterModel GetOneCourseMaster(int id)
        {
            CourseMaster Record = _unitOfWork.CourseMasterRepository.GetById(id);
            CourseMasterModel ReturnRecord = Mapper.CourseMasterMapper.CourseMasterDbToModel(Record);
            return ReturnRecord;
        }

      
        #endregion
    }
}

﻿using MasterInterface.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MasterInterface.Model;
using MasterRepository;

namespace MasterInterface.Implementation
{
    public class CommonService : ICommonService
    {
        private YMMasterEntities db;

        public int GetMenuId(string controllername)
        {
            try
            {
                using (db = new YMMasterEntities())
                {
                    var data = db.Menus.Where(a => a.Controller == controllername ).FirstOrDefault();
                    return data.Id;
                }
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        public RoleAccess GetRoleAccessByMenuIdAndRoleId(int menuId, int roleId)
        {

            using (db = new YMMasterEntities())
            {
                RoleAccess data = db.RoleAccesses.Where(a => a.MenuId == menuId && a.RoleId == roleId).FirstOrDefault();
                return data;
            }

        }


    }
}

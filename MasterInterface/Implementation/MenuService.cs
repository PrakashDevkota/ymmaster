﻿using MasterInterface.Interface;
using System.Collections.Generic;
using System.Linq;
using MasterInterface.Model;
using MasterRepository.UnitOfWork;
using MasterRepository;
using System.Web.Mvc;
using System.Web.Routing;
using System.Web;
using System;

namespace MasterInterface.Implementation
{
    public class MenuService : IMenuService
    {
        private IUnitOfWork _unityOfWork;
        public MenuService(UnitOfWork unityOfWork=null)
        {
            _unityOfWork = unityOfWork ?? new UnitOfWork();
        }

        //public List<MenuModel> GetDashBoardMenuList(int RoleId)
        //{
        //    string ControllerName = HttpContext.Current.Request.RequestContext.RouteData.Values["controller"].ToString().ToLower();
        //    string ActionName = HttpContext.Current.Request.RequestContext.RouteData.Values["action"].ToString().ToLower();
        //    int? ParentId = _unityOfWork.MenuRepository.Get(x => x.Controller.ToLower() == ControllerName && x.MenuLink.ToLower() == ActionName).Select(x => x.ParentId).FirstOrDefault();


        //    List<int> RoleMenu = _unityOfWork.RoleAccessRepository.Get(x => x.RoleId == RoleId).Select(x => x.MenuId).ToList();
        //    List<Menu> MenuModel = new List<Menu>();
        //    if (ParentId != null && ParentId != 0)
        //    {
        //        MenuModel = _unityOfWork.MenuRepository.All().Where(x => RoleMenu.Contains(x.Id) && x.ParentId != 0 && x.ParentId == ParentId).ToList();
        //    }
        //    else
        //    {
        //        MenuModel = _unityOfWork.MenuRepository.All().Where(x => RoleMenu.Contains(x.Id) && x.ParentId != 0 && x.IsDefault == true  && x.IsActive == true).ToList();
        //    }



        //    List<MenuModel> ReturnRecord = Mapper.MenuMapper.MenuDbToModelList(MenuModel);
        //    return ReturnRecord;
        //}


        //public List<MenuModel> GetDashBoardStudentMenuList(int RoleId)
        //{
        //    string ControllerName = HttpContext.Current.Request.RequestContext.RouteData.Values["controller"].ToString().ToLower();
        //    string ActionName = HttpContext.Current.Request.RequestContext.RouteData.Values["action"].ToString().ToLower();
        //    int? ParentId = _unityOfWork.MenuRepository.Get(x => x.Controller.ToLower() == ControllerName && x.MenuLink.ToLower() == ActionName).Select(x => x.ParentId).FirstOrDefault();


        //    List<int> RoleMenu = _unityOfWork.RoleAccessRepository.Get(x => x.RoleId == RoleId).Select(x => x.MenuId).ToList();
        //    List<Menu> MenuModel = new List<Menu>();
        //    if (ParentId != null && ParentId != 0)
        //    {
        //        MenuModel = _unityOfWork.MenuRepository.All().Where(x => RoleMenu.Contains(x.Id) && x.ParentId != 0 && x.ParentId == ParentId).ToList();
        //    }
        //    else
        //    {
        //        MenuModel = _unityOfWork.MenuRepository.All().Where(x => RoleMenu.Contains(x.Id) && x.ParentId != 0 && x.ParentId == 1037 && x.IsActive == true).ToList();
        //    }



        //    List<MenuModel> ReturnRecord = Mapper.MenuMapper.MenuDbToModelList(MenuModel);
        //    return ReturnRecord;
        //}

        //public List<MenuModel> GetDashBoardExamCenterMenuList(int RoleId)
        //{
        //    string ControllerName = HttpContext.Current.Request.RequestContext.RouteData.Values["controller"].ToString().ToLower();
        //    string ActionName = HttpContext.Current.Request.RequestContext.RouteData.Values["action"].ToString().ToLower();
        //    int? ParentId = _unityOfWork.MenuRepository.Get(x => x.Controller.ToLower() == ControllerName && x.MenuLink.ToLower() == ActionName).Select(x => x.ParentId).FirstOrDefault();


        //    List<int> RoleMenu = _unityOfWork.RoleAccessRepository.Get(x => x.RoleId == RoleId).Select(x => x.MenuId).ToList();
        //    List<Menu> MenuModel = new List<Menu>();
        //    if (ParentId != null && ParentId != 0)
        //    {
        //        MenuModel = _unityOfWork.MenuRepository.All().Where(x => RoleMenu.Contains(x.Id) && x.ParentId != 0 && x.ParentId == ParentId).ToList();
        //    }
        //    else
        //    {
        //        MenuModel = _unityOfWork.MenuRepository.All().Where(x => RoleMenu.Contains(x.Id) && x.ParentId != 0 && x.ParentId == 1041 && x.IsActive == true).ToList();
        //    }



        //    List<MenuModel> ReturnRecord = Mapper.MenuMapper.MenuDbToModelList(MenuModel);
        //    return ReturnRecord;
        //}


        //public List<MenuModel> GetDashBoardCourseMenuList(int RoleId)
        //{
        //    string ControllerName = HttpContext.Current.Request.RequestContext.RouteData.Values["controller"].ToString().ToLower();
        //    string ActionName = HttpContext.Current.Request.RequestContext.RouteData.Values["action"].ToString().ToLower();
        //    int? ParentId = _unityOfWork.MenuRepository.Get(x => x.Controller.ToLower() == ControllerName && x.MenuLink.ToLower() == ActionName).Select(x => x.ParentId).FirstOrDefault();


        //    List<int> RoleMenu = _unityOfWork.RoleAccessRepository.Get(x => x.RoleId == RoleId).Select(x => x.MenuId).ToList();
        //    List<Menu> MenuModel = new List<Menu>();
        //    if (ParentId != null && ParentId != 0)
        //    {
        //        MenuModel = _unityOfWork.MenuRepository.Get(x => RoleMenu.Contains(x.Id) && x.ParentId != x.Id && x.ParentId == ParentId).ToList();
        //    }
        //    else
        //    {
        //        MenuModel = _unityOfWork.MenuRepository.Get(x => RoleMenu.Contains(x.Id) && x.ParentId != x.ParentId && x.IsDefault == true).ToList();
        //    }



        //    List<MenuModel> ReturnRecord = Mapper.MenuMapper.MenuDbToModelList(MenuModel);
        //    return ReturnRecord;
        //}

        public List<MenuModel> GetAllActiveMenus(int RoleId)
        {
            string ControllerName = HttpContext.Current.Request.RequestContext.RouteData.Values["controller"].ToString().ToLower();
            string ActionName = HttpContext.Current.Request.RequestContext.RouteData.Values["action"].ToString().ToLower();
            int? ParentId = 0;
            if(ControllerName.ToLower() == "dashboard")
            {
                ParentId = _unityOfWork.MenuRepository.Get(x => x.Controller.ToLower() == ControllerName && x.MenuLink == ActionName).Select(x => x.ParentId).FirstOrDefault();
            }
            else
            {
                ParentId = _unityOfWork.MenuRepository.Get(x => x.Controller.ToLower() == ControllerName).Select(x => x.ParentId).FirstOrDefault();
            }

            List<int> RoleMenu = _unityOfWork.RoleAccessRepository.Get(x => x.RoleId == RoleId).Select(x => x.MenuId).ToList();
            List<Menu> MenuModel = new List<Menu>();
            if (ParentId != null && ParentId !=0)
            {
                MenuModel = _unityOfWork.MenuRepository.Get(x => RoleMenu.Contains(x.Id) && x.ParentId != x.Id && x.ParentId == ParentId).ToList();
            }
            else
            {
                MenuModel = _unityOfWork.MenuRepository.Get(x => RoleMenu.Contains(x.Id) && x.ParentId != x.ParentId && x.IsDefault == true).ToList();
            }
          
            List<MenuModel> ReturnRecord = Mapper.MenuMapper.MenuDbToModelList(MenuModel);
            return ReturnRecord;
        }
               
        public List<MenuModel> GetMainMenuList(int RoleId)
        {
            List<int> RoleMenu = _unityOfWork.RoleAccessRepository.Get(x => x.RoleId == RoleId).Select(x => x.MenuId).ToList();
            List<Menu> Record = _unityOfWork.MenuRepository.Get(x => x.IsActive == true && x.ParentId == x.Id && RoleMenu.Contains(x.Id)).OrderBy(x => x.Order).ToList();
            List<MenuModel> ReturnRecord = Mapper.MenuMapper.MenuDbToModelList(Record);
            return ReturnRecord; 
        }
    }
}

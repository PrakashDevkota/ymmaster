﻿using MasterInterface.Interface;
using MasterInterface.Model;
using MasterRepository;
using MasterRepository.UnitOfWork;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using MasterInterface.Helper;
using System.Security.Cryptography;
using System.Web.Security;
using System.Transactions;


namespace MasterInterface.Implementation
{
    public class UserService : IUserService
    {

        IEmailService _EmailService = new EmailService();
        #region Constructor
        private readonly IUnitOfWork _UnityOfWork;
        public UserService(IUnitOfWork unityOfWork=null)
        {
            _UnityOfWork = unityOfWork ?? new UnitOfWork();
        }

        #endregion

        #region Student UserModel
      

        public UserModel GetStudentUserModelCreateFor(int id)
        {
            Student Record = _UnityOfWork.StudentRepository.GetById(id);
            StudentModel StudentModelRecord = Mapper.StudentMapper.StudentDbToModel(Record);
            UserModel UserModelRecord = Mapper.UserMapper.StudentModelToUserModel(StudentModelRecord);
            UserModelRecord.Password = Membership.GeneratePassword(6, 2);
            return UserModelRecord;
        }
        public void AddStudentUser(UserModel Record)
        {
            LoginStudent ReturnRecord = Mapper.UserMapper.StudentUserModelToDb(Record);
            using (TransactionScope Scope =new TransactionScope())
            {
                using (MD5 md5Hash = MD5.Create())
                {
                    ReturnRecord.Password = HashPassword.GetMd5Hash(md5Hash, ReturnRecord.Password);
                    _UnityOfWork.LoginStudentRepository.Create(ReturnRecord);
                }
                _EmailService.SendMail(Record);
                Scope.Complete();
            }

          
            }
        
        


          
        public UserModel GetStudentUserModelUpdateFor(int id)
        {
            Student Record = _UnityOfWork.StudentRepository.GetById(id);
            StudentModel StudentModelRecord = Mapper.StudentMapper.StudentDbToModel(Record);
            UserModel UserModelRecord = Mapper.UserMapper.StudentModelToUserModel(StudentModelRecord);
            return UserModelRecord;
        }

        public void UpdateStudentUser(UserModel Record)
        {
           LoginStudent ReturnRecord = Mapper.UserMapper.StudentUserModelToDb(Record);
            _UnityOfWork.LoginStudentRepository.Update(ReturnRecord);
        }

        public void ResetStudentUser(int id)
        {
            using (TransactionScope Scope = new TransactionScope())
            {
                LoginStudent LoginStudentRecord = _UnityOfWork.LoginStudentRepository.Get(x => x.StudentId == id).FirstOrDefault(); ;
                LoginStudentRecord.Password = Membership.GeneratePassword(6, 2);
                UserModel ReturnRecord=Mapper.UserMapper.LoginStudentDbToModel(LoginStudentRecord);
                _EmailService.SendMail(ReturnRecord);
                using (MD5 md5Hash = MD5.Create())
                {
                    LoginStudentRecord.Password = HashPassword.GetMd5Hash(md5Hash, LoginStudentRecord.Password);
                    _UnityOfWork.LoginStudentRepository.Update(LoginStudentRecord);
                }
                 Scope.Complete();
            }
        }

        #endregion

        #region College UserModel
        public UserModel GetCollegeUserModelCreateFor(int id)
        {
            CourseInstituteBody Record = _UnityOfWork.CourseInstituteBodyRepository.GetById(id);
            CourseInstituteBodyModel CollegeModelRecord = Mapper.CourseInstituteBodyMapper.CourseInstituteBodyDbToModel(Record);
            UserModel UserModelRecord = Mapper.UserMapper.CourseInstituteBodyModelToUserModel(CollegeModelRecord);
            UserModelRecord.Password = Membership.GeneratePassword(6, 0);
            return UserModelRecord;
        }

        public void AddCollegeUser(UserModel Record)
        {
            LoginCollege ReturnRecord = Mapper.UserMapper.CollegeUserModelToDb(Record);
            using (TransactionScope Scope = new TransactionScope())
            {

                using (MD5 md5Hash = MD5.Create())
                {
                    ReturnRecord.Password = HashPassword.GetMd5Hash(md5Hash, ReturnRecord.Password);
                    _UnityOfWork.LoginCollegeRepository.Create(ReturnRecord);
                }
                _EmailService.SendMail(Record);
                Scope.Complete();
            }


        }
        public UserModel GetCollegeUserModelUpdateFor(int id)
        {
            LoginCollege LoginCollegeRecord = _UnityOfWork.LoginCollegeRepository.GetById(id);
            UserModel ReturnRecord = Mapper.UserMapper.CollegeLoginToUserModel(LoginCollegeRecord);
            return ReturnRecord;
        }

        public void UpdateCollegeUser(UserModel Record)
        {
            LoginCollege ReturnRecord = Mapper.UserMapper.CollegeUserModelToDb(Record);
            _UnityOfWork.LoginCollegeRepository.Update(ReturnRecord);
        }
        public void ResetCollegeUser(int id)
        {
            using (TransactionScope Scope = new TransactionScope())
            {
             
              LoginCollege  LoginCollegeRecord = _UnityOfWork.LoginCollegeRepository.GetById(id);
                LoginCollegeRecord.Password = Membership.GeneratePassword(6, 2);
                UserModel ReturnRecord = Mapper.UserMapper.LoginCollegeDbToModel(LoginCollegeRecord);
                _EmailService.SendMail(ReturnRecord);
                using (MD5 md5Hash = MD5.Create())
                {
                    LoginCollegeRecord.Password = HashPassword.GetMd5Hash(md5Hash, LoginCollegeRecord.Password);
                    _UnityOfWork.LoginCollegeRepository.Update(LoginCollegeRecord);
                }
                Scope.Complete();
            }

        }



        #endregion


        public IEnumerable<SelectListItem> GetStudentSelectList()
        {
            List<SelectListItem> sl = new List<SelectListItem>();
            List<Student> StudentList = _UnityOfWork.StudentRepository.All().ToList();
            foreach (Student item in StudentList)
            {
                sl.Add(new SelectListItem
                {
                    Value = item.Id.ToString(),
                    Text = item.FirstName + " " + item.MiddleName + " " + item.LastName
                });
            }
            return sl;
        }



    }
}

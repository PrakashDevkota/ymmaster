﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace MasterInterface.Helper
{
public   class StaticSelectlist
    {
        public static List<SelectListItem> GetGenderSelectList()
        {
            return new List<SelectListItem>
        {
            new SelectListItem{ Text="Male", Value = "M" },
            new SelectListItem{ Text="Female", Value = "F" },
            new SelectListItem{ Text="Other", Value = "O" },
        };
        }

        public static List<SelectListItem> GetBodyTypeSelectList()
        {
            return new List<SelectListItem>
        {
            new SelectListItem{ Text="Campus", Value = "C" },
            new SelectListItem{ Text="Affliated", Value = "A" },
        };
        }
        public static List<SelectListItem> GetNepaliMonthSelectList()
        {
            return new List<SelectListItem>
        {
              new SelectListItem{ Text="Baisakh", Value = "1" },
                   new SelectListItem{ Text="Jestha", Value = "2" },
                   new SelectListItem{ Text="Ashadh", Value = "3" },
                   new SelectListItem{ Text="Shrawan", Value = "4" },
                   new SelectListItem{ Text="Bhadra", Value = "5" },
                   new SelectListItem{ Text="Ashwin", Value = "6" },
                   new SelectListItem{ Text="Kartik", Value = "7" },
                   new SelectListItem{ Text="Mangsir", Value = "8" },
                   new SelectListItem{ Text="Poush", Value = "9" },
                   new SelectListItem{ Text="Magh", Value = "10" },
                   new SelectListItem{ Text="Fagun", Value = "11" },
                   new SelectListItem{ Text="Chaitra", Value = "12" },
        };
        }
    }
}

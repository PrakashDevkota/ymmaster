﻿using MasterInterface.Model;
using MasterRepository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MasterInterface.Mapper
{
  public  class StudentMapper
    {
        public static StudentModel StudentDbToModel(Student Record)
        {
            StudentModel ReturnRecord = new StudentModel
            {
                Id = Record.Id,
                Title = Record.Title,
                FirstName = Record.FirstName,
                MiddleName = Record.MiddleName,
                LastName = Record.LastName,
                StudentName= Record.FirstName+" "+Record.MiddleName+" "+Record.LastName,
                TURegisterationNumber = Record.TURegisterationNumber,
                CampusRollNumber = Record.CampusRollNumber,
                InstituteBodyId = Convert.ToInt32(Record.InstituteBodyId),
                FOSId = Convert.ToInt32(Record.FOSId),
                StatusId = Convert.ToInt32(Record.StatusId),
                DegreeId = Convert.ToInt32(Record.DegreeId),
                Sex = Record.Sex,
                DateOfBirth = Convert.ToDateTime(Record.DateOfBirth),
                FatherFullName = Record.FatherFullName,
                Email=Record.Email,
                PersonalContactNumber=Record.PersonalContactNumber,
                LocalGuardianFullName = Record.LocalGuardianFullName,
                AdmissionDate = Convert.ToDateTime(Record.AdmissionDate),
                CurrentAcedemicYear = Convert.ToInt32(Record.CurrentAcedemicYear),
                CurrentAcedemicPart = Convert.ToInt32(Record.CurrentAcedemicPart),
                ClearAll = Record.ClearAll,
                Affiliated=Record.Affiliated ,
                EntranceMarks=Record.EntranceMarks ,
                BarrierFlag=Record.BarrierFlag,
                BackPaperFlag=Record.BackPaperFlag ,
                RegularFullFee=Record.RegularFullFee ,
                PassYear =Record.PassYear,
                CompleteFlag=Record.CompleteFlag ,
                CompleteYear=Record.CompleteYear ,
                PTelephone = Record.PTelephone,
                PWardNumber = Convert.ToInt32(Record.PWardNumber),
                PTole = Record.PTole,
                PVillageCity = Record.PVillageCity,
                PDistrictId = Convert.ToInt32(Record.PDistrictId),
                PZoneId=Convert.ToInt32(Record.PZoneId),
                PCountryId = Convert.ToInt32(Record.PCountryId),
                CTelephone = Record.CTelephone,
                CWardNumber = Convert.ToInt32(Record.CWardNumber),
                CTole = Record.CTole,
                CVillageCity = Record.CVillageCity,
                CDistrictId = Convert.ToInt32(Record.CDistrictId),
                CZoneId=Convert.ToInt32(Record.CZoneId),
                CCountryId = Convert.ToInt32(Record.CCountryId),
                CreatedBy = Record.CreatedBy,
                CreatedOn = Record.CreatedOn,
                CourseDegree = new CourseDegreeModel
                {
                     DegreeName = Record.CourseDegree.DegreeName
                },
                CourseInstituteBody = new CourseInstituteBodyModel
                {
                   InstituteBodyName = Record.CourseInstituteBody.InstituteBodyName
                },
                CourseFieldOfStudy=new CourseFieldOfStudyModel
                {

                    Name=Record.CourseFieldOfStudy.Name
                }         


            };
            return ReturnRecord;
        }
        public static Student StudentModelToDb(StudentModel Record)
        {
            Student ReturnRecord = new Student
            {
                Id = Record.Id,
                Title = Record.Title,
                FirstName = Record.FirstName,
                MiddleName = Record.MiddleName,
                LastName = Record.LastName,
                TURegisterationNumber = Record.TURegisterationNumber,
                CampusRollNumber = Record.CampusRollNumber,
                InstituteBodyId = Convert.ToInt32(Record.InstituteBodyId),
                FOSId = Convert.ToInt32(Record.FOSId),
                StatusId = Convert.ToInt32(Record.StatusId),
                DegreeId = Convert.ToInt32(Record.DegreeId),
                Sex = Record.Sex,
                DateOfBirth = Convert.ToDateTime(Record.DateOfBirth),
                FatherFullName = Record.FatherFullName,
                Email = Record.Email,
                PersonalContactNumber = Record.PersonalContactNumber,
                LocalGuardianFullName = Record.LocalGuardianFullName,
                AdmissionDate = Convert.ToDateTime(Record.AdmissionDate),
                CurrentAcedemicYear = Convert.ToInt32(Record.CurrentAcedemicYear),
                CurrentAcedemicPart = Convert.ToInt32(Record.CurrentAcedemicPart),
                ClearAll = Record.ClearAll,
                Affiliated = Record.Affiliated,
                EntranceMarks = Record.EntranceMarks,
                BarrierFlag = Record.BarrierFlag,
                BackPaperFlag = Record.BackPaperFlag,
                RegularFullFee = Record.RegularFullFee,
                PassYear = Record.PassYear,
                CompleteFlag = Record.CompleteFlag,
                CompleteYear = Record.CompleteYear,
                PTelephone = Record.PTelephone,
                PWardNumber = Convert.ToInt32(Record.PWardNumber),
                PTole = Record.PTole,
                PVillageCity = Record.PVillageCity,
                PDistrictId = Convert.ToInt32(Record.PDistrictId),
                PZoneId = Convert.ToInt32(Record.PZoneId),
                PCountryId = Convert.ToInt32(Record.PCountryId),
                CTelephone = Record.CTelephone,
                CWardNumber = Convert.ToInt32(Record.CWardNumber),
                CTole = Record.CTole,
                CVillageCity = Record.CVillageCity,
                CDistrictId = Convert.ToInt32(Record.CDistrictId),
                CZoneId = Convert.ToInt32(Record.CZoneId),
                CCountryId = Convert.ToInt32(Record.CCountryId),
                CreatedBy = Record.CreatedBy,
                CreatedOn = Record.CreatedOn,
            };
            return ReturnRecord;
       }


        public static List<StudentModel> StudentDbToModelList(List<Student> Record)
        {
            List<StudentModel> sml = new List<StudentModel>();
            foreach (Student Row in Record)
            {
                StudentModel ReturnRecord = new StudentModel
                {
                    Id = Row.Id,
                    Title = Row.Title,
                    FirstName = Row.FirstName,
                    MiddleName = Row.MiddleName,
                    LastName = Row.LastName,
                    StudentName = Row.FirstName + " " + Row.MiddleName + " " + Row.LastName,
                    TURegisterationNumber = Row.TURegisterationNumber,
                    CampusRollNumber = Row.CampusRollNumber,
                    InstituteBodyId = Convert.ToInt32(Row.InstituteBodyId),
                    FOSId = Convert.ToInt32(Row.FOSId),
                    StatusId = Convert.ToInt32(Row.StatusId),
                    DegreeId = Convert.ToInt32(Row.DegreeId),
                    Sex = Row.Sex,
                    DateOfBirth = Convert.ToDateTime(Row.DateOfBirth),
                    FatherFullName = Row.FatherFullName,
                    Email = Row.Email,
                    PersonalContactNumber = Row.PersonalContactNumber,
                    LocalGuardianFullName = Row.LocalGuardianFullName,
                    AdmissionDate = Convert.ToDateTime(Row.AdmissionDate),
                    CurrentAcedemicYear = Convert.ToInt32(Row.CurrentAcedemicYear),
                    CurrentAcedemicPart = Convert.ToInt32(Row.CurrentAcedemicPart),
                    ClearAll = Row.ClearAll,
                    Affiliated = Row.Affiliated,
                    EntranceMarks = Row.EntranceMarks,
                    BarrierFlag = Row.BarrierFlag,
                    BackPaperFlag = Row.BackPaperFlag,
                    RegularFullFee = Row.RegularFullFee,
                    PassYear = Row.PassYear,
                    CompleteFlag = Row.CompleteFlag,
                    CompleteYear = Row.CompleteYear,
                    PTelephone = Row.PTelephone,
                    PWardNumber = Convert.ToInt32(Row.PWardNumber),
                    PTole = Row.PTole,
                    PVillageCity = Row.PVillageCity,
                    PDistrictId = Convert.ToInt32(Row.PDistrictId),
                    PZoneId = Row.PZoneId,
                    PCountryId = Convert.ToInt32(Row.PCountryId),
                    CTelephone = Row.CTelephone,
                    CWardNumber = Convert.ToInt32(Row.CWardNumber),
                    CTole = Row.CTole,
                    CVillageCity = Row.CVillageCity,
                    CDistrictId = Convert.ToInt32(Row.CDistrictId),
                    CZoneId = Row.CZoneId,
                    CCountryId = Convert.ToInt32(Row.CCountryId),
                    CreatedBy = Row.CreatedBy,
                    CreatedOn = Row.CreatedOn,


                    CourseDegree = new CourseDegreeModel
                    {
                        Id = Row.CourseDegree.Id,
                        DegreeName = Row.CourseDegree.DegreeName
                    },
                    CourseInstituteBody = new CourseInstituteBodyModel
                    {
                        Id = Row.CourseDegree.Id,
                        InstituteBodyName = Row.CourseInstituteBody.InstituteBodyName
                    },
                    CourseFieldOfStudy = new CourseFieldOfStudyModel
                    {
                        Id = Row.CourseFieldOfStudy.Id,
                        Name = Row.CourseFieldOfStudy.Name
                    },

                };
                sml.Add(ReturnRecord);
            }
            return sml;
        }





    }
}

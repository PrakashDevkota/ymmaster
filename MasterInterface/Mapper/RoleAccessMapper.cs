﻿using MasterInterface.Model;
using MasterRepository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MasterInterface.Mapper
{
  public  class RoleAccessMapper
    {
        public static RoleAccessModel RoleAccessDbToModel(RoleAccess Record)
        {
            RoleAccessModel ReturnRecord = new RoleAccessModel
            {
                Id = Record.Id,
                RoleId=Record.RoleId,
                MenuId=-Record.MenuId,
                AllowAdd=Record.AllowAdd,
                AllowDelete=Record.AllowDelete,
                AllowEdit=Record.AllowEdit,
                AllowView=Record.AllowView
               
            };
            return ReturnRecord;
        }
        public static RoleAccess RoleAccessModelToDb(RoleAccessModel Record)
        {
            RoleAccess ReturnRecord = new RoleAccess
            {
                Id = Record.Id,
                RoleId = Record.RoleId,
                MenuId = -Record.MenuId,
                AllowAdd = Record.AllowAdd,
                AllowDelete = Record.AllowDelete,
                AllowEdit = Record.AllowEdit,
                AllowView = Record.AllowView
            };
            return ReturnRecord;
        }


        public static List<RoleAccessModel> RoleAccessDbToModelList(List<RoleAccess> Record)
        {
            List<RoleAccessModel> cml = new List<RoleAccessModel>();
            foreach (RoleAccess Row in Record)
            {
                RoleAccessModel ReturnRecord = new RoleAccessModel
                {
                    Id = Row.Id,
                    Role = new RoleModel
                    {
                        Name =Row.Role.Name
                    },
                    Menu=new MenuModel
                    {
                        Name=Row.Menu.Name
                    },
                    AllowAdd=Row.AllowAdd,
                    AllowDelete = Row.AllowDelete,
                    AllowEdit = Row.AllowEdit,
                    AllowView = Row.AllowView

                };
                cml.Add(ReturnRecord);
            }
            return cml;
        }





    }
}

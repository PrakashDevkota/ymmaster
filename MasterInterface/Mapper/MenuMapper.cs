﻿using MasterInterface.Model;
using MasterRepository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MasterInterface.Mapper
{
  public class MenuMapper
    {
        public static MenuModel MenuDbToModel(Menu Record)
        {
            MenuModel ReturnRecord = new MenuModel
            {
                Id = Record.Id,
                ParentId=Record.ParentId,
                Name = Record.Name,
                CssClass=Record.CssClass,
                MenuLink=Record.MenuLink,
                Order=Record.Order,
                Controller=Record.Controller,
                IsDefault=Record.IsDefault,
                IsActive=Record.IsActive

            };
            return ReturnRecord;
        }
        public static Menu MenuModelToDb(MenuModel Record)
        {
            Menu ReturnRecord = new Menu
            {
                Id = Record.Id,
                ParentId = Convert.ToInt32(Record.ParentId),
                Name = Record.Name,
                CssClass = Record.CssClass,
                MenuLink = Record.MenuLink,
                Order = Record.Order,
                Controller = Record.Controller,
                IsDefault = Record.IsDefault,
                IsActive = Record.IsActive
            };
            return ReturnRecord;
        }


        public static List<MenuModel> MenuDbToModelList(List<Menu> Record)
        {
            List<MenuModel> cml = new List<MenuModel>();
            foreach (Menu Row in Record)
            {
                MenuModel ReturnRecord = new MenuModel
                {
                    Id = Row.Id,
                    ParentId = Row.ParentId,
                    Name = Row.Name,
                    CssClass = Row.CssClass,
                    MenuLink = Row.MenuLink,
                    Order = Row.Order,
                    Controller = Row.Controller,
                    IsDefault = Row.IsDefault,
                    IsActive = Row.IsActive
                };
                cml.Add(ReturnRecord);
            }
            return cml;
        }


    }
}

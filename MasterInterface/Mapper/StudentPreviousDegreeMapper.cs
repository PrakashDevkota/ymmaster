﻿using MasterInterface.Model;
using MasterRepository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MasterInterface.Mapper
{
  public class StudentPreviousDegreeMapper
    {
        public static StudentPreviousDegreeModel StudentPreviousDegreeDbToModel(StudentPreviousDegree Record)
        {
            StudentPreviousDegreeModel ReturnRecord = new StudentPreviousDegreeModel
            {
             
                Id = Record.Id,
                DegreeLevelId = Record.DegreeLevelId,
                CampusRollNo = Record.CampusRollNo,
                InstituteBodyId=Record.InstituteBodyId,
                Board = Record.Board,
                Examination = Record.Examination,
                Year=Record.Year,
                FullMarks=Record.FullMarks,
                TotalMarksObtain=Record.TotalMarksObtain,
                Migration=Record.Migration,
                SchoolCollege=Record.SchoolCollege,
                Program=Record.Program,
                Division=Record.Division,
                 CreatedBy = Record.CreatedBy,
                CreatedOn = Record.CreatedOn
            };
            return ReturnRecord;
        }
        public static StudentPreviousDegree StudentPreviousDegreeModelToDb(StudentPreviousDegreeModel Record)
        {
            StudentPreviousDegree ReturnRecord = new StudentPreviousDegree
            {
                Id = Record.Id,
                DegreeLevelId = Record.DegreeLevelId,
                CampusRollNo = Record.CampusRollNo,
                InstituteBodyId = Record.InstituteBodyId,
                Board = Record.Board,
                Examination = Record.Examination,
                Year = Convert.ToInt32(Record.Year),
                FullMarks = Convert.ToDecimal(Record.FullMarks),
                TotalMarksObtain = Convert.ToDecimal(Record.TotalMarksObtain),
                Migration = Record.Migration,
                SchoolCollege = Record.SchoolCollege,
                Program = Record.Program,
                Division = Record.Division,
                CreatedBy = Record.CreatedBy,
                CreatedOn = Record.CreatedOn
            };
            return ReturnRecord;
        }


        public static List<StudentPreviousDegreeModel> StudentPreviousDegreeDbToModelList(List<StudentPreviousDegree> Record)
        {
            List<StudentPreviousDegreeModel> cml = new List<StudentPreviousDegreeModel>();
            foreach (StudentPreviousDegree Row in Record)
            {
                StudentPreviousDegreeModel ReturnRecord = new StudentPreviousDegreeModel
                {
                    Id = Row.Id,
                    DegreeLevelId = Row.DegreeLevelId,
                    CampusRollNo = Row.CampusRollNo,
                    InstituteBodyId = Row.InstituteBodyId,
                    Board = Row.Board,
                    Examination = Row.Examination,
                    Year = Row.Year,
                    FullMarks = Row.FullMarks,
                    TotalMarksObtain = Row.TotalMarksObtain,
                    Migration = Row.Migration,
                    SchoolCollege = Row.SchoolCollege,
                    Program = Row.Program,
                    Division = Row.Division,
                    CreatedBy = Row.CreatedBy,
                    CreatedOn = Row.CreatedOn,
                    CourseDegreeLevel=new CourseDegreeLevelModel
                    {
                        Id=Row.CourseDegreeLevel.Id,
                        DegreeLevelName=Row.CourseDegreeLevel.DegreeLevelName
                    },
                    CourseInstituteBody=new CourseInstituteBodyModel
                    {
                        InstituteBodyName=Row.CourseInstituteBody.InstituteBodyName
                    }
                };
                cml.Add(ReturnRecord);
            }
            return cml;
        }





    }
}

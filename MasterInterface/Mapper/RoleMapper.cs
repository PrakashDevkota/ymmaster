﻿using MasterInterface.Model;
using MasterRepository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MasterInterface.Mapper
{
   public class RoleMapper
    {
        public static RoleModel RoleDbToModel(Role Record)
        {
            RoleModel ReturnRecord = new RoleModel
            {
                Id = Record.Id,
                Name = Record.Name,
                Detail=Record.Detail,
                AccessAll=Record.AccessAll
              
            
            };
            return ReturnRecord;
        }
        public static Role RoleModelToDb(RoleModel Record)
        {
            Role ReturnRecord = new Role
            {
                Id = Record.Id,
                Name = Record.Name,
                Detail = Record.Detail,
                AccessAll = Record.AccessAll
            };
            return ReturnRecord;
        }


        public static List<RoleModel> RoleDbToModelList(List<Role> Record)
        {
            List<RoleModel> rml = new List<RoleModel>();
            foreach (Role Row in Record)
            {
                RoleModel ReturnRecord = new RoleModel
                {
                    Id = Row.Id,
                    Name = Row.Name,
                    Detail = Row.Detail,
                    AccessAll = Row.AccessAll
                };
                rml.Add(ReturnRecord);
            }
            return rml;
        }





    }
}

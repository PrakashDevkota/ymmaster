﻿using MasterInterface.Model;
using MasterRepository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MasterInterface.Mapper
{
    public class LoginMapper
    {
        //public static LoginModel LoginDbToModel(Login Record)
        //{
        //    LoginModel ReturnRecord = new LoginModel
        //    {
        //        Id = Record.Id,
        //        UserName = Record.UserName,
        //        Password = Record.Password,
        //        ReturnUrl = Record.ReturnUrl,
        //         RoleId = Record.RoleId,
        //        Role = new RoleModel
        //        {
        //            Id = Record.Id,
        //            Name = Record.Role.Name
        //        },
        //        InstituteBodyId = Record.InstituteBodyId,
        //        StudentId = Record.StudentId,
        //        IsActive = Record.IsActive,
        //        CreatedBy = Record.CreatedBy,
        //        CreatedOn = Record.CreatedOn
        //    };
        //    return ReturnRecord;
        //}
        //public static Login LoginModelToDb(LoginModel Record)
        //{
        //    Login ReturnRecord = new Login
        //    {
        //        Id = Record.Id,
        //        UserName = Record.UserName,
        //        Password = Record.Password,
        //        ReturnUrl = Record.ReturnUrl,
        //        RoleId = Record.Id,
        //        InstituteBodyId = Record.InstituteBodyId,
        //        StudentId = Record.Student.Id,
        //        IsActive = Record.IsActive,
        //        CreatedBy = Record.CreatedBy,
        //        CreatedOn = Record.CreatedOn

        //    };
        //    return ReturnRecord;
        //}


        public static LoginModel LoginSuperAdminDbToModel(UserDetail Record)
        {
            LoginModel ReturnRecord = new LoginModel
            {
                Id = Record.Id,
                UserName = Record.Username,
                Password = Record.Password,
                ReturnUrl = Record.ReturnUrl,
                RoleId = Convert.ToInt32(Record.RoleId),
                Role = new RoleModel
                {
                    Id = (int)Record.RoleId,
                   
                    //Name=Record.Role.Name,

                },
                IsActive = (bool)Record.IsActive,
                //CreatedBy = (int)Record.CreatedBy,
                //CreatedDate = (DateTime)Record.CreatedDate
            };
            return ReturnRecord;
        }
        //public static LoginSuperAdmin LoginSuperAdminModelToDb(LoginModel Record)
        //{
        //    LoginSuperAdmin ReturnRecord = new LoginSuperAdmin
        //    {
        //        Id = Record.Id,
        //        UserName = Record.UserName,
        //        Password = Record.Password,
        //        ReturnUrl = Record.ReturnUrl,
        //        RoleId = Record.Id,
        //        IsActive = Record.IsActive,
        //        CreatedBy = Record.CreatedBy,
        //        CreatedOn = Record.CreatedOn

        //    };
        //    return ReturnRecord;
        //}


        //public static LoginModel LoginStudentDbToModel(LoginStudent Record)
        //{
        //    LoginModel ReturnRecord = new LoginModel
        //    {
        //        Id = Record.Id,
        //        UserName = Record.UserName,
        //        Password = Record.Password,
        //        ReturnUrl = Record.ReturnUrl,
        //        RoleId = Record.RoleId,
        //        Role = new RoleModel
        //        {
        //            Id = Record.Id,
        //            Name = Record.Role.Name
        //        },
        //         StudentId = Record.StudentId,
        //        IsActive = Record.IsActive,
        //        CreatedBy = Record.CreatedBy,
        //        CreatedOn = Record.CreatedOn
        //    };
        //    return ReturnRecord;
        //}
        //public static LoginStudent LoginStudentModelToDb(LoginModel Record)
        //{
        //    LoginStudent ReturnRecord = new LoginStudent
        //    {
        //        Id = Record.Id,
        //        UserName = Record.UserName,
        //        Password = Record.Password,
        //        ReturnUrl = Record.ReturnUrl,
        //        RoleId = Record.Id,
        //        StudentId = Record.Student.Id,
        //        IsActive = Record.IsActive,
        //        CreatedBy = Record.CreatedBy,
        //        CreatedOn = Record.CreatedOn

        //    };
        //    return ReturnRecord;
        //}
        //public static LoginModel LoginCollegeDbToModel(LoginCollege Record)
        //{
        //    LoginModel ReturnRecord = new LoginModel
        //    {
        //        Id = Record.Id,
        //        UserName = Record.UserName,
        //        Password = Record.Password,
        //        ReturnUrl = Record.ReturnUrl,
        //        RoleId = Record.RoleId,
        //        Role = new RoleModel
        //        {
        //            Id = Record.Id,
        //            Name = Record.Role.Name
        //        },
        //        InstituteBodyId = Record.InstituteBodyId,
        //        IsActive = Record.IsActive,
        //        CreatedBy = Record.CreatedBy,
        //        CreatedOn = Record.CreatedOn
        //    };
        //    return ReturnRecord;
        //}
        //public static LoginCollege LoginCollegeModelToDb(LoginModel Record)
        //{
        //    LoginCollege ReturnRecord = new LoginCollege
        //    {
        //        Id = Record.Id,
        //        UserName = Record.UserName,
        //        Password = Record.Password,
        //        ReturnUrl = Record.ReturnUrl,
        //        RoleId = Record.Id,
        //        InstituteBodyId = Record.InstituteBodyId,
        //        IsActive = Record.IsActive,
        //        CreatedBy = Record.CreatedBy,
        //        CreatedOn = Record.CreatedOn

        //    };
        //    return ReturnRecord;
        //}
    }
}

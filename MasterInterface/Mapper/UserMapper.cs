﻿using MasterInterface.Model;
using MasterRepository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace MasterInterface.Mapper
{
 public  class UserMapper
    {
        //public static UserModel LoginDbToModel(Login Record)
        //{
        //    UserModel ReturnRecord = new UserModel
        //    {
        //        Id = Record.Id,
        //        UserName = Record.UserName,
        //        Password = Record.Password,
        //        RoleId = Record.RoleId,
        //        Role = new RoleModel
        //        {
        //            Id = Record.Id,
        //            Name = Record.Role.Name
        //        },
        //        InstituteBodyId = Record.InstituteBodyId,
        //        StudentId = Record.StudentId,
        //        IsActive = Record.IsActive,
        //        CreatedBy = Record.CreatedBy,
        //        CreatedOn = Record.CreatedOn
        //    };
        //    return ReturnRecord;
        //}

        //public static Login UserModelToDb(UserModel Record)
        //{
        //    Login ReturnRecord = new Login
        //    {
        //        Id = Record.Id,
        //        UserName = Record.UserName,
        //        Password = Record.Password,
        //        RoleId = Record.Id,
        //        InstituteBodyId = Record.InstituteBodyId,
        //        StudentId = Record.Student.Id,
        //        IsActive = Record.IsActive,
        //        CreatedBy = Record.CreatedBy,
        //        CreatedOn = Record.CreatedOn

        //    };
        //    return ReturnRecord;
        //}

        //public static Login CollegeUserModelToDb(UserModel Record)
        //{
        //    Login ReturnRecord = new Login
        //    {
        //        Id = Record.Id,
        //        UserName = Record.UserName,
        //        Password = Record.Password,
        //        RoleId = Record.RoleId,
        //        InstituteBodyId = Record.InstituteBodyId,
        //        IsActive = Record.IsActive,
        //        CreatedBy = Record.CreatedBy,
        //        CreatedOn = Record.CreatedOn

        //    };
        //    return ReturnRecord;
        //}
        //public static UserModel CollegeLoginToUserModel(Login LoginRecord)
        //{
        //    UserModel UserModelRecord = new UserModel
        //    {

        //        Id = LoginRecord.Id,
        //        UserName = LoginRecord.UserName,
        //        Password = LoginRecord.Password,
        //        RoleId = LoginRecord.RoleId,
        //        InstituteBodyId = LoginRecord.InstituteBodyId,
        //        StudentId = LoginRecord.StudentId,
        //        IsActive = LoginRecord.IsActive,
        //        CreatedBy = LoginRecord.CreatedBy,
        //        CreatedOn=LoginRecord.CreatedOn
        //    };
        //    return UserModelRecord;
        //}

        //public static Login StudentUserModelToDb(UserModel Record)
        //{
        //    Login ReturnRecord = new Login
        //    {
        //        Id = Record.Id,
        //        UserName = Record.UserName,
        //        Password = Record.Password,
        //        RoleId = Record.RoleId,
        //        StudentId = Record.StudentId,
        //        IsActive = Record.IsActive,
        //        CreatedBy = Record.CreatedBy,
        //        CreatedOn = Record.CreatedOn

        //    };
        //    return ReturnRecord;
        //}

        //public static UserModel LoginSuperAdminDbToModel(LoginSuperAdmin Record)
        //{
        //    UserModel ReturnRecord = new UserModel
        //    {
        //        Id = Record.Id,
        //        UserName = Record.UserName,
        //        Password = Record.Password,
        //        RoleId = Record.RoleId,
        //        Role = new RoleModel
        //        {
        //            Id = Record.Id,
        //            Name = Record.Role.Name
        //        },
        //        IsActive = Record.IsActive,
        //        CreatedBy = Record.CreatedBy,
        //        CreatedOn = Record.CreatedOn
        //    };
        //    return ReturnRecord;
        //}



        //public static UserModel LoginStudentDbToModel(LoginStudent Record)
        //{
        //    UserModel ReturnRecord = new UserModel
        //    {
        //        Id = Record.Id,
        //        UserName = Record.UserName,
        //        Password = Record.Password,
        //        RoleId = Record.RoleId,
        //        Role = new RoleModel
        //        {
        //            Id = Record.Id,
        //            Name = Record.Role.Name
        //        },
        //        IsActive = Record.IsActive,
        //        CreatedBy = Record.CreatedBy,
        //        CreatedOn = Record.CreatedOn
        //    };
        //    return ReturnRecord;
        //}

        //public static UserModel LoginCollegeDbToModel(LoginCollege Record)
        //{
        //    UserModel ReturnRecord = new UserModel
        //    {
        //        Id = Record.Id,
        //        UserName = Record.UserName,
        //        Password = Record.Password,
        //        RoleId = Record.RoleId,
        //        Role = new RoleModel
        //        {
        //            Id = Record.Id,
        //            Name = Record.Role.Name
        //        },
        //        IsActive = Record.IsActive,
        //        CreatedBy = Record.CreatedBy,
        //        CreatedOn = Record.CreatedOn
        //    };
        //    return ReturnRecord;
        //}

        //public static LoginSuperAdmin UserModelToDb(UserModel Record)
        //{
        //    LoginSuperAdmin ReturnRecord = new LoginSuperAdmin
        //    {
        //        Id = Record.Id,
        //        UserName = Record.UserName,
        //        Password = Record.Password,
        //        RoleId = Record.Id,
        //        //InstituteBodyId = Record.InstituteBodyId,
        //        //StudentId = Record.Student.Id,
        //        IsActive = Record.IsActive,
        //        CreatedBy = Record.CreatedBy,
        //        CreatedOn = Record.CreatedOn

        //    };
        //    return ReturnRecord;
        //}

        //public static LoginCollege CollegeUserModelToDb(UserModel Record)
        //{
        //    LoginCollege ReturnRecord = new LoginCollege
        //    {
        //        Id = Record.Id,
        //        UserName = Record.UserName,
        //        Password = Record.Password,
        //        RoleId = Record.RoleId,
        //        InstituteBodyId = Record.InstituteBodyId,
        //        IsActive = Record.IsActive,
        //        CreatedBy = Record.CreatedBy,
        //        CreatedOn = Record.CreatedOn

        //    };
        //    return ReturnRecord;
        //}
        //public static UserModel CollegeLoginToUserModel(LoginCollege LoginRecord)
        //{
        //    UserModel UserModelRecord = new UserModel
        //    {

        //        Id = LoginRecord.Id,
        //        UserName = LoginRecord.UserName,
        //        Password = LoginRecord.Password,
        //        RoleId = LoginRecord.RoleId,
        //        InstituteBodyId = LoginRecord.InstituteBodyId,
        //        IsActive = LoginRecord.IsActive,
        //        CreatedBy = LoginRecord.CreatedBy,
        //        CreatedOn = LoginRecord.CreatedOn
        //    };
        //    return UserModelRecord;
        //}

        //public static LoginStudent StudentUserModelToDb(UserModel Record)
        //{
        //    LoginStudent ReturnRecord = new LoginStudent
        //    {
        //        Id = Record.Id,
        //        UserName = Record.UserName,
        //        Password = Record.Password,
        //        RoleId = Record.RoleId,
        //        StudentId = Record.StudentId,
        //        IsActive = Record.IsActive,
        //        CreatedBy = Record.CreatedBy,
        //        CreatedOn = Record.CreatedOn

        //    };
        //    return ReturnRecord;
        //}



        //(Record.Id + Record.InstituteBodyName).ToString()

        //public static UserModel CourseInstituteBodyModelToUserModel(CourseInstituteBodyModel Record)
        //{
        //      UserModel userModel = new UserModel
        //    {
        //        InstituteBodyId = Record.Id,
        //        UserName = Record.Email,
        //        Password = Record.Email,
        //        RoleId = 3,
        //        CourseInstituteBody =new CourseInstituteBodyModel
        //        {
        //            InstituteBodyName=Record.InstituteBodyName
        //        },
        //        IsActive = Record.IsActive,
        //        CreatedBy = Record.CreatedBy,
        //        CreatedOn = Record.CreatedOn
        //    };
        //    return userModel;
        //}


        //public static UserModel StudentModelToUserModel(StudentModel Record)
        //{

        //    UserModel userModel = new UserModel()
        //    {
        //        StudentId = Record.Id,
        //        UserName = Record.Email,
        //        Password = Record.TURegisterationNumber + Record.FirstName,
        //        RoleId = 2,
        //        Student = new StudentModel
        //        {
        //            FirstName = Record.FirstName,
        //            MiddleName = Record.MiddleName,
        //            LastName = Record.LastName
        //        },
        //        IsActive=Record.IsActive,
        //        CreatedBy = Record.CreatedBy,
        //        CreatedOn = Record.CreatedOn

        //    };

        //    return userModel;    
        //}
}
}

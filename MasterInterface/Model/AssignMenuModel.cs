﻿using MasterInterface.Model;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MasterInterface.Model
{
  public  class AssignMenuModel
    {
        public int RoleId { get; set; }
        public List<MenuModel> Menu { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace MasterInterface.Model
{
    public class MenuModel
    {
        public int Id { get; set; }
        public int? ParentId { get; set; }
        public string Name { get; set; }
        public string CssClass { get; set; }
        public string MenuLink { get; set; }
        public int Order { get; set; }
        public string Controller { get; set; }
        public bool IsDefault { get; set; }
        public bool IsActive { get; set; }
        public IEnumerable<SelectListItem> ParentMenu { get; set; }

        public bool IsSelected { get; set; }
        public bool Create { get; set; }
        public bool Read { get; set; }
        public bool Update { get; set; }
        public bool Delete { get; set; }

    }
}

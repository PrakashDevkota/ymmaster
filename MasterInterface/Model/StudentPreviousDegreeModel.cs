﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace MasterInterface.Model
{
     public  class StudentPreviousDegreeModel
    {
        public int Id { get; set; }
        public int DegreeLevelId { get; set; }
        public string CampusRollNo { get; set; }
        public int InstituteBodyId { get; set; }
        [Required]
        public string Board { get; set; }
        [Required]
        public string Examination { get; set; }
        [Required]
        public int? Year { get; set; }
        [Required]
        public decimal? FullMarks { get; set; }
        [Required]
        public decimal? TotalMarksObtain { get; set; }
        [Required]
        public bool Migration { get; set; }
        [Required]
        public string SchoolCollege { get; set; }
        [Required]
        public string Program { get; set; }
        [Required]
        public string Division { get; set; }
        public int CreatedBy { get; set; }
        public DateTime CreatedOn { get; set; }

        public IEnumerable<SelectListItem> DegreeLevelSelectList { get; set; }
        public IEnumerable<SelectListItem> InstituteBodySelectList { get; set; }

        //public  CourseDegreeLevelModel CourseDegreeLevel { get; set; }
        //public  CourseInstituteBodyModel CourseInstituteBody { get; set; }
    }
}

﻿using MasterRepository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MasterInterface.Model
{
 public   class RoleAccessModel
    {
        public int Id { get; set; }
        public int RoleId { get; set; }
        public int MenuId { get; set; }
        public bool AllowAdd { get; set; }
        public bool AllowEdit { get; set; }
        public bool AllowDelete { get; set; }
        public bool AllowView { get; set; }

        public IEnumerable<Menu> MenuSelectList { get; set; }
        public IEnumerable<Role> RoleSelectList { get; set; }

        public  MenuModel Menu { get; set; }
        public  RoleModel Role { get; set; }
    }
}

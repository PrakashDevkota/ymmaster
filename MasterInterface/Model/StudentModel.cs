﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace MasterInterface.Model
{
    public class StudentModel
    {
       
        public int Id { get; set; }
        [Required]
        public string Title { get; set; }
        [Required]
        [Display(Name = "First Name")]
        public string FirstName { get; set; }
        [Display(Name = "Middle Name")]
        public string MiddleName { get; set; }
        [Required]
        [Display(Name = "Last Name")]
        public string LastName { get; set; }
        [Required]
        [Display(Name = "TU Registration Number")]
        public string TURegisterationNumber { get; set; }
        [Required]
        [Display(Name = "Campus Roll Number")]
        public string CampusRollNumber { get; set; }
        [Display(Name = "Institute Body")]
        public int? InstituteBodyId { get; set; }
        [Display(Name = "Field Of Study")]
        public int? FOSId { get; set; }
        [Display(Name = "Status")]
        public int? StatusId { get; set; }
        [Display(Name = "Degree")]
        public int? DegreeId { get; set; }
        [Required]
        public string Sex { get; set; }
        [Required]
        [Display(Name = "Date Of Birth")]
        public DateTime? DateOfBirth { get; set; }
        [Required]
        [Display(Name = "Father Name")]
        public string FatherFullName { get; set; }
        [Required]
        [DataType(DataType.EmailAddress)]
        public string Email { get; set; }
        [Required]
        [Display(Name = "Personal Contact Number")]
        public string PersonalContactNumber { get; set; }
        [Required]
        [Display(Name = "Local Guardian Name")]
        public string LocalGuardianFullName { get; set; }
        [Required]
        [Display(Name = "Local Guardian Telephone")]
        public string LocalGuardianTelephone { get; set; }
        [Required]
        [Display(Name = "Local Guardian Address")]
        public string LocalGuradianAddress { get; set; }
        [Required]
        [DataType(DataType.Date)]
        [Display(Name = "Admssion Date")]
        public DateTime? AdmissionDate { get; set; }
        [Required]
        [Display(Name = "Currnet Academic Year")]
        public int? CurrentAcedemicYear { get; set; }
        [Required]
        [Display(Name = "Current Academic Part")]
        public int? CurrentAcedemicPart { get; set; }
        [Required]
        [Display(Name = "Clear All")]
        public bool ClearAll { get; set; }
        public string Affiliated { get; set; }
    
        [Display(Name = "Entrance Marks")]
        public Nullable<decimal> EntranceMarks { get; set; }
        [Required]
        [Display(Name = "Barrier Flag")]
        public bool BarrierFlag { get; set; }
        [Required]
        [Display(Name = "Back Paper Flag")]
        public bool BackPaperFlag { get; set; }
        [Required]
        [Display(Name = "Regular Full Fee")]
        public string RegularFullFee { get; set; }
       
        [Display(Name = "Pass Year")]
        public Nullable<int> PassYear { get; set; }
        [Required]
        [Display(Name = "Complete Flag")]
        public bool CompleteFlag { get; set; }
        [Display(Name = "Complete Year")]
        public Nullable<int> CompleteYear { get; set; }
        [Display(Name = "Permanent Telephone")]
        public string PTelephone { get; set; }
     
        [Display(Name = "Permanent Ward Number")]
        public int? PWardNumber { get; set; }
        [Display(Name = "Permanaent Tole")]
        public string PTole { get; set; }
        public string PVillageCity { get; set; }
        public int? PDistrictId { get; set; }
        public int? PZoneId { get; set; }
        public int? PCountryId { get; set; }
        public string CTelephone { get; set; }
        public int? CWardNumber { get; set; }
        public string CTole { get; set; }
        public string CVillageCity { get; set; }
        public int? CDistrictId { get; set; }
        public int? CZoneId { get; set; }
        public int? CCountryId { get; set; }
        public int CreatedBy { get; set; }
        public DateTime CreatedOn { get; set; }
        [Display(Name = "Name")]
        public string FullName { get { return FirstName + " " + MiddleName + " " + LastName; } set { } }
        public string StudentName { get; set; }
        //Update student user 
        public bool IsActive { get; set; }
        public bool IsUserCreated { get; set; }

        public IEnumerable<SelectListItem> CourseDegreeSelectList { get; set; }
        public IEnumerable<SelectListItem> CourseFieldOfStudySelectList { get; set; }
        public IEnumerable<SelectListItem> CourseInstituteBodySelectList { get; set; }
        public IEnumerable<SelectListItem> StudentSexSelectList { get; set; }

        //public  CourseDegreeModel CourseDegree { get; set; }
        //public  CourseFieldOfStudyModel CourseFieldOfStudy { get; set; }
        //public  CourseInstituteBodyModel CourseInstituteBody { get; set; }
    }

    public class StudentWithFilter
    {
        public List<StudentModel> Students { get; set; }
        [Required]
        [DisplayName("Institute")]
        public Nullable<int> InstituteBodyId { get; set; }
        [DisplayName("Degree")]
        public Nullable<int> CourseDegreeId { get; set; }
        [DisplayName("Student")]
        public Nullable<int> StudentId { get; set; }
        public List<SelectListItem> InstituteSelectList { get; set; }
        public List<SelectListItem> DegreeSelectList { get; set; }
        public List<SelectListItem> StudentSelectList { get; set; }
    }
}

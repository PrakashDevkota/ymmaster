﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace MasterInterface.Model
{
  public  class LoginModel
    {
        public int Id { get; set; }
        [Required]
        public string UserName { get; set; }
        [Required]
        [DataType(DataType.Password)]
        public string Password { get; set; }

        [HiddenInput]
        public string ReturnUrl { get; set; }
        [Display(Name = "Role")]
        public int RoleId { get; set; }
        [Display(Name = "Institute Body")]
    
        public bool IsActive { get; set; }

        public DateTime CreatedDate { get; set; }
        public int CreatedBy { get; set; }
     //   public CourseInstituteBodyModel CourseInstituteBody { get; set; }
        public virtual RoleModel Role { get; set; }
      
    }
}

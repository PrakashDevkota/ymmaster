﻿using MasterRepository.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MasterRepository.UnitOfWork
{
    public class UnitOfWork : IUnitOfWork
    {
        YMMasterEntities _context = null;
        public UnitOfWork()
        {
            _context = new YMMasterEntities();
        }
        public UnitOfWork(YMMasterEntities dbcontext)
        {
            _context = dbcontext;
        }

        public YMMasterEntities Context { get { return _context; } }
       
        private IRepository<Menu> _MenuRepository;
        private IRepository<Role> _RoleRepository;
      
        private IRepository<RoleAccess> _RoleAccessRepository;
        private IRepository<UserDetail> _UserDetailRepository;


        public IRepository<Menu> MenuRepository
        {
            get { return _MenuRepository ?? (_MenuRepository = new RepositoryBase<Menu>(_context)); }
        }

        public IRepository<Role> RoleRepository
        {
            get { return _RoleRepository ?? (_RoleRepository = new RepositoryBase<Role>(_context)); }
        }

     
        public IRepository<RoleAccess> RoleAccessRepository
        {
            get { return _RoleAccessRepository ?? (_RoleAccessRepository = new RepositoryBase<RoleAccess>(_context)); }
        }

        public IRepository<UserDetail> UserDetailRepository
        {
            get
            {
                return _UserDetailRepository ?? (_UserDetailRepository = new RepositoryBase<UserDetail>(_context));
            }
        }

        public int Save()
        {
            try
            {
                return _context.SaveChanges();
            }
            catch (System.Data.Entity.Core.OptimisticConcurrencyException ex)
            {
                throw ex;
            }
        }
       
    }
}

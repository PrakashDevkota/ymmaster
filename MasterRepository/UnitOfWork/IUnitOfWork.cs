﻿
using MasterRepository.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MasterRepository.UnitOfWork
{
    public interface IUnitOfWork
    {
   
        IRepository<Menu> MenuRepository { get; }
        IRepository<Role> RoleRepository { get; }
        IRepository<RoleAccess> RoleAccessRepository { get; }
       IRepository<UserDetail> UserDetailRepository { get; }
        int Save();
     
    }
}

﻿using MasterInterface.Implementation;
using MasterInterface.Interface;
using MasterInterface.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using YMMaster.Controllers;

namespace IOEExamRegistration.Controllers.Setup
{  [HasCompanyId]
    public class RoleController : Controller
    {
        private ISetupService _SetupService;

        public RoleController()
        {
            _SetupService = new SetupService();
        }
        public ActionResult Index()
        {
            List<RoleModel> Record = _SetupService.GetAllRoles();
            return View("../Setup/Role/Index", Record);
        }

        public ActionResult Create()
        {
            RoleModel Record = new RoleModel();
            return View("../Setup/Role/Create", Record);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(RoleModel Record)
        {

            try
            {
                if (ModelState.IsValid)
                {
                    _SetupService.AddRole(Record);
                    TempData["Success"] = "Role added successfully";
                    return RedirectToAction("Index");
                }
                else
                {
                    TempData["Danger"] = "Form error";
                }
            }
            catch (Exception Exception)
            {
                TempData["Danger"] = Exception.Message;

            }
            return View("../Setup/Role/Create", Record);
        }


        [HttpGet]
        public ActionResult Update(int Id)
        {
            RoleModel Record = new RoleModel();
            Record = _SetupService.GetOneRole(Id);
            return View("../Setup/Role/Update", Record);

        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Update(RoleModel Record)
        {
           try
            {
                if (ModelState.IsValid)
                {
                    _SetupService.UpdateRole(Record);
                    TempData["Success"] = "Role updated successfully";
                    return RedirectToAction("Index");
                }
                else
                {
                    TempData["Danger"] = "Form error";
                }
            }
            catch (Exception Exception)
            {
                TempData["Danger"] = Exception.Message;
            }

            return View("../Setup/Role//Update", Record);
        }


    }
}
﻿using IOEInterface.Implementation;
using IOEInterface.Interface;
using IOEInterface.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Web;
using System.Web.Mvc;

namespace IOEExamRegistration.Controllers.ExamFeeStructure
{
    [Authorize(Roles = "SuperAdmin")]
    public class ExamFeeStructureController : Controller
    {
        private IExamService _Examservice;
        public ExamFeeStructureController()
        {
            _Examservice = new ExamService();
        }
        public ActionResult Index()
        {
            List<ExamFeeStructureModel> Record = _Examservice.GetAllExamFeeStructures();
            return View("../Setup/ExamFeeStructure/Index", Record);
        }
        public ActionResult Create()
        {
            ExamFeeStructureModel Record = new ExamFeeStructureModel();
            return View("../Setup/ExamFeeStructure/Create", Record);
        }

        [HttpPost]
        public ActionResult Create(ExamFeeStructureModel Record)
        {
              try
            {
                if (ModelState.IsValid)
                {
                    _Examservice.AddExamFeeStructure(Record);
                    TempData["Success"] = "ExamFeeStructure added successfully";
                    return RedirectToAction("Index");
                }
                else
                {
                    TempData["Danger"] = "Form error";
                }
            }
            catch (Exception Exception)
            {
                TempData["Danger"] = Exception.Message;
                return RedirectToAction("Index");

            }
            return View("../Setup/ExamFeeStructure/Create", Record);
        }

        [HttpGet]
        public ActionResult Update(int Id)
        {
            ExamFeeStructureModel Record = new ExamFeeStructureModel();
            Record = _Examservice.GetOneExamFeeStructure(Id);
            return View("../Setup/ExamFeeStructure/Update", Record);

        }

        [HttpPost]
        public ActionResult Update(ExamFeeStructureModel Record)
        {
              try
            {
                if (ModelState.IsValid)
                {
                    _Examservice.UpdateExamFeeStructure(Record);
                    TempData["Success"] = "ExamFeeStructure updated successfully";
                    return RedirectToAction("Index");
                }
                else
                {
                    TempData["Danger"] = "Form error";
                }
            }
            catch (Exception Exception)
            {
                TempData["Danger"] = Exception.Message;
                return RedirectToAction("Index");
            }

            return View("../Setup/ExamFeeStructure/Update", Record);
        }

       
    }
}
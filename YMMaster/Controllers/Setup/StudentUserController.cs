﻿using MasterInterface.Implementation;
using MasterInterface.Interface;
using MasterInterface.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Web;
using System.Web.Mvc;

namespace IOEExamRegistration.Controllers.Setup
{
    [Authorize(Roles = "SuperAdmin,IOE")]
    public class StudentUserController : Controller
    {
        IStudentService _StudentService;
        IUserService _UserService;


        public StudentUserController()
        {
            _StudentService = new StudentService();
            _UserService = new UserService();
        }
        // GET: StudentUser
        public ActionResult Index()
        {
            StudentModel Record = new StudentModel();
            Record.CourseInstituteBodySelectList = _StudentService.GetInsituteBodyList();
            return View("../Setup/StudentUser/Index", Record);
        }

        [HttpGet]
        public ActionResult Get()
        {
            List<StudentModel> Record = new List<StudentModel>();
            return View("../Setup/StudentUser/Get", Record);
        }

        [HttpPost]
        public ActionResult Get(int InstituteBodyId = 0)
        {
            List<StudentModel> Record = _StudentService.GetAllStudentByInstituteBodyId(InstituteBodyId);
            return View("../Setup/StudentUser/Get", Record);
               }

       


        public ActionResult Create(int Id)
        {
            UserModel Record = new UserModel();
            Record = _UserService.GetStudentUserModelCreateFor(Id);
            var LoginId = ((ClaimsIdentity)User.Identity).FindFirst("LoginId").Value;
            Record.CreatedBy = Convert.ToInt32(LoginId);
            Record.CreatedOn = DateTime.Now;
            return View("../Setup/StudentUser/Create", Record);
        }

        [HttpPost]
        public ActionResult Create(UserModel Record)
        {
            var LoginId = ((ClaimsIdentity)User.Identity).FindFirst("LoginId").Value;
            Record.CreatedBy = Convert.ToInt32(LoginId);
            Record.CreatedOn = DateTime.Now;
            try
            {
                if (ModelState.IsValid)
                {
                    _UserService.AddStudentUser(Record);
                    TempData["Success"] = "Student User added successfully";
                    return RedirectToAction("Get");
                }
                else
                {
                    TempData["Danger"] = "Form error";

                }
            }
            catch (Exception Exception)
            {
                TempData["Danger"] = Exception.Message;
                return RedirectToAction("Get");
            }

            return View("../Setup/StudentUser/Create", Record);
        }

        public ActionResult Update(int Id)
        {
            UserModel Record = new UserModel();
            Record = _UserService.GetStudentUserModelUpdateFor(Id);
            var LoginId = ((ClaimsIdentity)User.Identity).FindFirst("LoginId").Value;
            Record.CreatedBy = Convert.ToInt32(LoginId);
            Record.CreatedOn = DateTime.Now;
            return View("../Setup/StudentUser/Update", Record);
        }


        [HttpPost]
        public ActionResult Update(UserModel Record)
        {
            Record.StudentSelectListItem = _UserService.GetStudentSelectList();
            try
            {
                if (ModelState.IsValid)
                {
                    _UserService.UpdateStudentUser(Record);
                    TempData["Success"] = "Student User updated successfully";
                    return RedirectToAction("Get");
                }
                else
                {
                    TempData["Danger"] = "Form error";
                }
            }
            catch (Exception Exception)
            {
                TempData["Danger"] = Exception.Message;
                return RedirectToAction("Get");
            }

            return View("../Setup/StudentUser/Update", Record);
        }

        public ActionResult Reset(int Id)
        {
            _UserService.ResetStudentUser(Id);
            TempData["Success"] = "Password reset successfully";
            return RedirectToAction("Get");
        }

    }
}
﻿using MasterInterface.Implementation;
using MasterInterface.Interface;
using MasterInterface.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Web;
using System.Web.Mvc;

namespace IOEExamRegistration.Controllers.Setup
{
    [Authorize(Roles = "SuperAdmin,IOE")]
    public class CollegeUserController : Controller
    {
        private IUserService _UserService = null;
        private ICourseService _CourseService = null;
        public CollegeUserController()
        {
            _UserService = new UserService();
            _CourseService = new CourseService();
        }

        // GET: CollegeUser
        public ActionResult Index()
        {
            List<CourseInstituteBodyModel> Record = _CourseService.GetAllCourseInstituteBody();
            return View("../Setup/CollegeUser/Index", Record);
        }

        public ActionResult Create(int Id)
        {
            UserModel Record = new UserModel();
            Record = _UserService.GetCollegeUserModelCreateFor(Id);
            var LoginId = ((ClaimsIdentity)User.Identity).FindFirst("LoginId").Value;
            Record.CreatedBy = Convert.ToInt32(LoginId);
            Record.CreatedOn = DateTime.Now;
            
            return View("../Setup/CollegeUser/Create", Record);
        }

        [HttpPost]
        public ActionResult Create(UserModel Record)
        {
            
            Record.CourseInstituteBodySelectListItem = _CourseService.GetCourseInstituteBodySelectList();
            try
            {
                if (ModelState.IsValid)
                {
                    _UserService.AddCollegeUser(Record);
                    TempData["Success"] = "Institute Body added successfully";
                    return RedirectToAction("Index");
                }
                else
                {
                    TempData["Danger"] = "Form error";
                }
            }
            catch (Exception Exception)
            {
                TempData["Danger"] = Exception.Message;
                return RedirectToAction("Index");
            }

            return View("../Setup/CollegeUser/Create", Record);
        }


        public ActionResult Update(int Id)
        {
            UserModel Record = new UserModel();
            Record = _UserService.GetCollegeUserModelUpdateFor(Id);
            return View("../Setup/CollegeUser/Update", Record);
        }

        [HttpPost]
        public ActionResult Update(UserModel Record)
        {
            
            try
            {
                if (ModelState.IsValid)
                {
                    _UserService.UpdateCollegeUser(Record);
                    TempData["Success"] = "Institute Body updated successfully";
                    return RedirectToAction("Index");
                }
                else
                {
                    TempData["Danger"] = "Form error";
                }
            }
            catch (Exception Exception)
            {
                TempData["Danger"] = Exception.Message;
                return RedirectToAction("Index");
            }

            return View("../Setup/CollegeUser/Update", Record);
        }

        public ActionResult Reset(int Id)
        {
            _UserService.ResetCollegeUser(Id);
            TempData["Success"] = "Password reset successfully";
            return RedirectToAction("Index");
        }
    }
}
﻿using MasterInterface.Implementation;
using MasterInterface.Interface;
using MasterInterface.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace IOEExamRegistration.Controllers.Setup
{
    public class UserLoginController : Controller
    {
        private ISetupService _SetupService;
        public UserLoginController()
        {
            _SetupService = new SetupService();
        }

        public ActionResult Create()
        {
           
            return View();
        }
        [HttpPost]
        public ActionResult Create(UserModel Record)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    _SetupService.AddUser(Record);
                    TempData["Success"] = "User added successfully";
                    return RedirectToAction("Index");
                }
                else
                {
                    TempData["Danger"] = "Form error";
                }
            }
            catch (Exception Exception)
            {
                TempData["Danger"] = Exception.Message;

            }
            return View("../Setup/User/Create", Record);
        }

    }
}
﻿using MasterInterface.Implementation;
using MasterInterface.Interface;
using MasterInterface.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace IOEExamRegistration.Controllers.Setup
{
    
    public class MenuController : Controller
    {
        private ISetupService _SetupService;
    

           public MenuController()
        {
            _SetupService = new SetupService();
            
        }
        public ActionResult Index()
        {
            List<MenuModel> Record = _SetupService.GetAllMenus();
            return View("../Setup/Menu/Index", Record);
        }
        
        public ActionResult Create()
        {
            MenuModel Record = new MenuModel();
            Record.ParentMenu = _SetupService.GetParentMenus();
            return View("../Setup/Menu/Create", Record);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(MenuModel Record)
        {
           
            Record.ParentMenu = _SetupService.GetParentMenus();
            try
            {
                if (ModelState.IsValid)
                {
                    _SetupService.AddMenu(Record);
                    TempData["Success"] = "Menu added successfully";
                    return RedirectToAction("Index");
                }
                else
                {
                    TempData["Danger"] = "Form error";
                }
            }
            catch (Exception Exception)
            {
                TempData["Danger"] = Exception.Message;

            }
            return View("../Menu/MenuSetup/Create", Record);
        }

       
        [HttpGet]
        public ActionResult Update(int Id)
        {
            MenuModel Record = new MenuModel();
            Record = _SetupService.GetOneMenu(Id);
            Record.ParentMenu = _SetupService.GetParentMenus();
            return View("../Setup/Menu/Update", Record);

        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Update(MenuModel Record)
        {
            Record.ParentMenu = _SetupService.GetParentMenus();
            try
            {
                if (ModelState.IsValid)
                {
                    _SetupService.UpdateMenu(Record);
                    TempData["Success"] = "Menu updated successfully";
                    return RedirectToAction("Index");
                }
                else
                {
                    TempData["Danger"] = "Form error";
                }
            }
            catch (Exception Exception)
            {
                TempData["Danger"] = Exception.Message;
            }

            return View("../Setup/Menu//Update", Record);
        }

      


    }
}
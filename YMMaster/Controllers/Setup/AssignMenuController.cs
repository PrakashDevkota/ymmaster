﻿using MasterInterface.Implementation;
using MasterInterface.Interface;
using MasterInterface.Model;
using MasterRepository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Web;
using System.Web.Mvc;


namespace YMMaster.Controllers.Setup
{
   [HasCompanyId]
    public class AssignMenuController : Controller
    {
        private YMMasterEntities db;
        private ISetupService _SetupService;

        public AssignMenuController()
        {
            db = new YMMasterEntities();
        _SetupService = new SetupService();
    }
        //Get Role List
        public ActionResult Index()

        {
            List<RoleModel> Record=_SetupService.GetAllRoles();
            return View("../Setup/AssignMenu/Index",Record);
        }

        //Get Menu List 
        [HttpGet]
        public ActionResult RoleMenuAccessSetup(int id)
        {
            AssignMenuModel Record = _SetupService.GetRoleAccess(id);
            var data = db.Roles.Find(id);
            ViewBag.RoleName = data.Name;
            return View("../Setup/AssignMenu/RoleMenuAccessSetup", Record);
        }

        [HttpPost]
        public ActionResult RoleMenuAccessSetup(AssignMenuModel Record)
        {
            try
            {
                _SetupService.AddRoleAccess(Record);
                TempData["Success"] = "Role Assigned successfully";
            }
            catch (Exception Exception)
            {
                 TempData["Danger"] = Exception.Message;
                 return RedirectToAction("Index");
            }
            return View("../Setup/AssignMenu/RoleMenuAccessSetup",Record);
        }
    }
}
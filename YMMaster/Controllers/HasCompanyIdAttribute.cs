﻿using MasterInterface.Implementation;
using MasterInterface.Interface;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace YMMaster.Controllers
{
    public class HasCompanyIdAttribute : ActionFilterAttribute
    {
        public ICommonService _CommonService;


        public HasCompanyIdAttribute()
        {

            _CommonService = new CommonService();

        }

        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {/* check every action on each controller*/
            HttpContext context = HttpContext.Current;
            int roleID = (int)(context.Session["RoleId"]);
            string ControllerName = HttpContext.Current.Request.RequestContext.RouteData.Values["controller"].ToString().ToLower();
            string ActionName = HttpContext.Current.Request.RequestContext.RouteData.Values["action"].ToString().ToLower();
            int menuId = _CommonService.GetMenuId(ControllerName);
            var data = _CommonService.GetRoleAccessByMenuIdAndRoleId(menuId, roleID);
            switch (ActionName) {
                case "index":
                    if(data.AllowView!=true)filterContext.Result = new RedirectToRouteResult(new RouteValueDictionary(new { action = "UnAuthorized", controller = "DashBoard" }));
                    break;
                case "create":
                    if (data.AllowAdd != true) filterContext.Result = new RedirectToRouteResult(new RouteValueDictionary(new { action = "UnAuthorized", controller = "DashBoard" }));
                    break;
                case "update":
                    if (data.AllowEdit != true) filterContext.Result = new RedirectToRouteResult(new RouteValueDictionary(new { action = "UnAuthorized", controller = "DashBoard" }));
                    break;
                case "delete":
                    if (data.AllowDelete != true) filterContext.Result = new RedirectToRouteResult(new RouteValueDictionary(new { action = "UnAuthorized", controller = "DashBoard" }));
                    break;
                 default:
                   // filterContext.Result = new RedirectToRouteResult(new RouteValueDictionary(new { action = "Index", controller = "Menu" }));

                    break;
            }
        }
    }
}
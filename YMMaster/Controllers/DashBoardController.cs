﻿using YMMaster.Models;
using MasterInterface.Implementation;
using MasterInterface.Interface;

using MasterInterface.Model;
using MasterRepository;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.OleDb;
using System.Linq;
using System.Security.Claims;
using System.Web;
using System.Web.Mvc;
using System.Xml;

namespace YMMaster.Controllers
{
    
    public class DashBoardController : Controller
    {
        private IMenuService _MenuService;
        private ICommonService _Commonservice;

        private YMMasterEntities db;
        public DashBoardController()
        {
            _Commonservice = new CommonService();
            _MenuService = new MenuService();
        }

        public ActionResult Index()
        {
            var RoleId = ((ClaimsIdentity)User.Identity).FindFirst("RoleId").Value;
            List<MenuModel> Record = _MenuService.GetAllActiveMenus(Convert.ToInt32(RoleId));
            return View(Record);

        }



        public ActionResult Student()
        {
            var RoleId = ((ClaimsIdentity)User.Identity).FindFirst("RoleId").Value;
            List<MenuModel> Record = _MenuService.GetAllActiveMenus(Convert.ToInt32(RoleId));
            return View(Record);

        }
        public ActionResult Admin()
        {
            var RoleId = ((ClaimsIdentity)User.Identity).FindFirst("RoleId").Value;
            List<MenuModel> Record = _MenuService.GetAllActiveMenus(Convert.ToInt32(RoleId));
            return View(Record);

        }
        public ActionResult College()
        {
            var RoleId = ((ClaimsIdentity)User.Identity).FindFirst("RoleId").Value;
            List<MenuModel> Record = _MenuService.GetAllActiveMenus(Convert.ToInt32(RoleId));
            return View(Record);

        }
        public ActionResult Menu()
        {
            var RoleId = ((ClaimsIdentity)User.Identity).FindFirst("RoleId").Value;
            List<MenuModel> Record = _MenuService.GetAllActiveMenus(Convert.ToInt32(RoleId));
            return View(Record);

        }


        public ActionResult Course()
        {
            var RoleId = ((ClaimsIdentity)User.Identity).FindFirst("RoleId").Value;
            List<MenuModel> Record = _MenuService.GetAllActiveMenus(Convert.ToInt32(RoleId));
            return View(Record);

        }

        public ActionResult UnAuthorized()
        {
            return View();
        }
       
  }
}
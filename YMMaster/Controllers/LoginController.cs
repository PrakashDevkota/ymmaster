﻿using MasterInterface.Implementation;
using MasterInterface.Interface;
using MasterInterface.Model;

using System;
using System.Collections.Generic;
using System.Linq;

using System.Security.Claims;
using System.Web;

using System.Web.Mvc;
using System.Web.SessionState;

namespace YMMaster.Controllers
{
    [AllowAnonymous]
    public class LoginController : Controller
    {
       private ILoginService _LoginService;
       private IMenuService _MenuService;
        public LoginController()
        {
            _LoginService = new LoginService();
          _MenuService = new MenuService();
        }
       

        [HttpGet]
        public ActionResult Index(string returnUrl)
        {
            var model = new LoginModel
            {
                ReturnUrl = returnUrl
            };
            if (!string.IsNullOrEmpty(returnUrl))
            {
                return RedirectToAction("Index","DashBoard");
            }
            return View(model);
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Index(LoginModel model)
        {
            if (!ModelState.IsValid)
            {
                return View();
            }

            var auth= _LoginService.GetLoginDetails(model);
        

            if (auth.Id > 0)
            {
                var identity = new ClaimsIdentity(new[] {
                new Claim(ClaimTypes.Name, auth.UserName),
                new Claim(ClaimTypes.Email, auth.UserName),
                new Claim(ClaimTypes.Role, auth.UserName),
                new Claim("RoleId", (auth.RoleId).ToString()),
                new Claim("LoginId",(auth.Id).ToString())

            },
                    "ApplicationCookie");

                var ctx = Request.GetOwinContext();
                var authManager = ctx.Authentication;

                authManager.SignIn(identity);

                
                Session["RoleId"] = auth.RoleId;
                return Redirect(GetRedirectUrl(model.ReturnUrl));
            }

            // user authN failed
            ModelState.AddModelError("", "Invalid email or password");
            return View();
        }

        private string GetRedirectUrl(string returnUrl)
        {
            if (string.IsNullOrEmpty(returnUrl) || !Url.IsLocalUrl(returnUrl))
            {
                return Url.Action("Index", "DashBoard");
            }
            return returnUrl;
        }

        public ActionResult LogOut()
        {
            var ctx = Request.GetOwinContext();
            var authManager = ctx.Authentication;

            authManager.SignOut("ApplicationCookie");
            return RedirectToAction("Index");
        }

        public ActionResult Sidebar()
        {
            var RoleId = ((ClaimsIdentity)User.Identity).FindFirst("RoleId").Value;
            List<MenuModel> Record = _MenuService.GetAllActiveMenus(Convert.ToInt32(RoleId));
            return View(Record);
        }

        public ActionResult GetMainMenuList()
        {
            var RoleId = ((ClaimsIdentity)User.Identity).FindFirst("RoleId").Value;
            List<MenuModel> Record = _MenuService.GetMainMenuList(Convert.ToInt32(RoleId));
            return View(Record);
        }
    }
}
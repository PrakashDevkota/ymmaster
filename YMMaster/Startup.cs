﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(YMMaster.Startup))]
namespace YMMaster
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace YMMaster.Models
{
    public class VoucherViewModel
    {

        public int Id { get; set; }
        public string Uni_RollNo { get; set; }
        public string LastName { get; set; }
        public string VoucherNo { get; set; }
        public string VoucherDate { get; set; }
        public Nullable<decimal> VoucherAmount { get; set; }
        public string Status { get; set; }
    }
}